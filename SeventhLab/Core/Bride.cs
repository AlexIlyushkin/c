﻿namespace Ex_1
{
    class Bride : Person
    {
        #region Attributes
        private BrideProperties properties;
        #endregion

        #region Constructors
        public Bride() : base()
        {
            this.properties = BrideProperties.NULL;
        }
                 
        public Bride(string firstName, string secondName, int age, BrideProperties properties) : base(firstName, secondName, age)
        {
            this.properties = properties;
        }
        #endregion

        #region Methods
        public bool HasOnly(BrideProperties patternProperties)
        {
            return (this.properties & patternProperties) == patternProperties && (this.properties & ~patternProperties) == 0;
        }

        public bool HasAll(BrideProperties patternPropertie)
        {
            return (this.properties & patternPropertie) == patternPropertie;
        }

        public bool HasSome(BrideProperties patternPropertie)
        {
            return (this.properties & patternPropertie) > 0 && (this.properties & patternPropertie) < patternPropertie;
        }

        public bool HasNo(BrideProperties patternPropertie)
        {
            return (~this.properties & patternPropertie) == patternPropertie;
        }
        #endregion

        #region Properties
        public BrideProperties Properties
        {
            set
            {
                properties = value;
            }
            get
            {
                return properties;
            }
        }
        #endregion
    }
}
