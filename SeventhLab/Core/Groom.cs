﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_1
{
    class Groom : Person
    {
        #region Attributes
        private GroomProperties properties;
        #endregion

        #region Constructors
        public Groom() : base() { }
        
        public Groom(string firstName, string secondName, int age, GroomProperties properties) : base(firstName, secondName, age)
        {
            Properties = properties;            
        }
        #endregion

        #region Methods
        public bool HasOnly(GroomProperties patternProperties)
        {
            return (this.properties & patternProperties) == patternProperties && (this.properties & ~patternProperties) == 0;
        }

        public bool HasAll(GroomProperties patternProperties)
        {
            return (this.properties & patternProperties) == patternProperties;
        }

        public bool HasSome(GroomProperties patternProperties)
        {
            return (this.properties & patternProperties) > 0 && (this.properties & patternProperties) < patternProperties;
        }

        public bool HasNo(GroomProperties patternProperties)
        {
            return (~this.properties & patternProperties) == patternProperties;
        }
        #endregion

        #region Properties
        public GroomProperties Properties
        {
            set
            {
                properties = value;
            }
            get
            {
                return properties;
            }
        }
        #endregion
    }
}
