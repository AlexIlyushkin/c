﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex_1
{
    class GroomManager
    {
        #region Attributes
        private GroomProperties positiveProperties;
        private GroomProperties negativeProperties;

        private List<Groom> allGrooms;
        private List<Groom> groomsHavingOnlyPositiveProperties;
        private List<Groom> groomsHavingAllPositiveProperties;
        private List<Groom> groomsHavingSomePositiveProperties;
        #endregion

        #region Constructors
        public GroomManager()
        {
            positiveProperties = GroomProperties.NULL;
            negativeProperties = GroomProperties.NULL;

            allGrooms = new List<Groom>()
            {
                new Groom("Алексей", "Илюшкин", 18, GroomProperties.beautiful | GroomProperties.bold | GroomProperties.clever | GroomProperties.fat | GroomProperties.poor),
                new Groom("Арнольд", "Шварцнегер", 71, GroomProperties.ugly | GroomProperties.bold | GroomProperties.stupid | GroomProperties.rich | GroomProperties.athletic),
                new Groom("Джаред", "Лето", 47, GroomProperties.beautiful | GroomProperties.skinny | GroomProperties.rich | GroomProperties.clever | GroomProperties.cowardly),
                new Groom("Вася", "Пупкин", 564563, GroomProperties.ugly | GroomProperties.fat | GroomProperties.angry | GroomProperties.cowardly | GroomProperties.poor | GroomProperties.selfish | GroomProperties.stupid),
                new Groom("Невася", "Непупкин", 242352, GroomProperties.beautiful | GroomProperties.athletic | GroomProperties.kind | GroomProperties.bold | GroomProperties.rich | GroomProperties.caring | GroomProperties.clever)
            };
        }
        #endregion

        #region Methods
        public void SelectGrooms()
        {
            groomsHavingOnlyPositiveProperties = new List<Groom>(5);
            groomsHavingAllPositiveProperties = new List<Groom>(5);
            groomsHavingSomePositiveProperties = new List<Groom>(5);

            for(int i = 0; i < allGrooms.Count; i++)
            {
                if(allGrooms[i].HasOnly(positiveProperties))
                {
                    groomsHavingOnlyPositiveProperties.Add(allGrooms[i]);
                }
                else if(allGrooms[i].HasAll(positiveProperties) && allGrooms[i].HasNo(negativeProperties))
                {
                    groomsHavingAllPositiveProperties.Add(allGrooms[i]);
                }
                else if(allGrooms[i].HasSome(positiveProperties) && allGrooms[i].HasNo(negativeProperties))
                {
                    groomsHavingSomePositiveProperties.Add(allGrooms[i]);
                }
            }
        }                      

        public void AddPositiveProperty(GroomProperties property)
        {
            PositiveProperties |= property;
        }

        public void RemovePositiveProperty(GroomProperties property)
        {
            PositiveProperties ^= property;
        }

        public void AddNegativeProperty(GroomProperties property)
        {
            NegativeProperties |= property;
        }

        public void RemoveNegativeProperty(GroomProperties property)
        {
            NegativeProperties ^= property;
        }
        #endregion

        #region Properties
        public List<Groom> AllGrooms
        {
            set
            {
                allGrooms = value;
            }
            get
            {
                return allGrooms;
            }
        }

        public List<Groom> GroomsHavingOnlyPositiveProperties
        {
            set
            {
                groomsHavingOnlyPositiveProperties = value;
            }
            get
            {
                return groomsHavingOnlyPositiveProperties;
            }
        }
        
        public List<Groom> GroomsHavingPositiveProperties
        {
            set
            {
                groomsHavingAllPositiveProperties = value;
            }
            get
            {
                return groomsHavingAllPositiveProperties;
            }
        }

        public List<Groom> GroomsHavingSomePositiveProperties
        {
            set
            {
                groomsHavingSomePositiveProperties = value;
            }
            get
            {
                return groomsHavingSomePositiveProperties;
            }
        }

        public GroomProperties PositiveProperties
        {
            set
            {
                positiveProperties = value;
            }
            get
            {
                return positiveProperties;
            }
        }

        public GroomProperties NegativeProperties
        {
            set
            {
                negativeProperties = value;
            }
            get
            {
                return negativeProperties;
            }
        }
        #endregion
    }
}
