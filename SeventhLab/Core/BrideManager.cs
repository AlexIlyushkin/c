﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex_1
{
    class BrideManager
    {
        #region Attributes
        private BrideProperties positiveProperties;
        private BrideProperties negativeProperties;

        private List<Bride> allBrides;
        private List<Bride> bridesHavingOnlyPositiveProperties;
        private List<Bride> bridesHavingAllPositiveProperties;
        private List<Bride> bridesHavingSomePositiveProperties;
        #endregion

        #region Constructors
        public BrideManager()
        {
            positiveProperties = BrideProperties.NULL;
            negativeProperties = BrideProperties.NULL;

            AllBrides = new List<Bride>()
            {
                new Bride("Элеонора", "Ахметшина", 666, BrideProperties.breastSize_1 | BrideProperties.stupid | BrideProperties.angry | BrideProperties.beautiful | BrideProperties.skinny),
                new Bride("Саша", "Грей", 31, BrideProperties.beautiful | BrideProperties.breastSize_2 | BrideProperties.rich | BrideProperties.athletic),
                new Bride("Доздроперма", "Ворфоломеева", 63, BrideProperties.ugly | BrideProperties.stupid | BrideProperties.breastSize_0 | BrideProperties.angry | BrideProperties.fat | BrideProperties.poor),
                new Bride("Несуществует", "Такой", 21, BrideProperties.athletic | BrideProperties.beautiful | BrideProperties.breastSize_3 | BrideProperties.clever | BrideProperties.kind | BrideProperties.rich)
            };
        }
        #endregion

        #region Methods
        public void SelectBrides()
        {
            bridesHavingOnlyPositiveProperties = new List<Bride>(5);
            bridesHavingAllPositiveProperties = new List<Bride>(5);
            bridesHavingSomePositiveProperties = new List<Bride>(5);

            for (int i = 0; i < allBrides.Count; i++)
            {
                if(allBrides[i].HasOnly(positiveProperties))
                {
                    bridesHavingOnlyPositiveProperties.Add(allBrides[i]);
                }
                else if(allBrides[i].HasAll(positiveProperties) && allBrides[i].HasNo(negativeProperties))
                {
                    bridesHavingAllPositiveProperties.Add(allBrides[i]);
                }
                else if(allBrides[i].HasSome(positiveProperties) && allBrides[i].HasNo(negativeProperties)) 
                {
                    bridesHavingSomePositiveProperties.Add(allBrides[i]);
                }
            }
        }                     

        public void AddPositiveProperty(BrideProperties property)
        {
            PositiveProperties |= property;
        }

        public void RemovePositiveProperty(BrideProperties property)
        {
            PositiveProperties ^= property;
        }

        public void AddNegativeProperty(BrideProperties property)
        {
            NegativeProperties |= property;
        }

        public void RemoveNegativeProperty(BrideProperties property)
        {
            NegativeProperties ^= property;
        }
        #endregion

        #region Properties
        public List<Bride> AllBrides
        {
            set
            {
                allBrides = value;
            }
            get
            {
                return allBrides;
            }
        }

        public List<Bride> BridesHavingOnlyPositiveProperties
        {
            set
            {
                bridesHavingOnlyPositiveProperties = value;
            }
            get
            {
                return bridesHavingOnlyPositiveProperties;
            }
        }

        public List<Bride> BridesHavingAllPositiveProperties
        {
            set
            {
                bridesHavingAllPositiveProperties = value;
            }
            get
            {
                return bridesHavingAllPositiveProperties;
            }
        }

        public List<Bride> BridesHavingSomePositiveProperties
        {
            set
            {
                bridesHavingSomePositiveProperties =value;
            }
            get
            {
                return bridesHavingSomePositiveProperties;
            }
        }

        public BrideProperties PositiveProperties
        {
            set
            {
                positiveProperties = value;
            }
            get
            {
                return positiveProperties;
            }
        }

        public BrideProperties NegativeProperties
        {
            set
            {
                negativeProperties = value;
            }
            get
            {
                return negativeProperties;
            }
        }
        #endregion
    }
}
