﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_1
{
    [Flags]
    public enum GroomProperties
    {
        NULL = 0,
        kind = 1,
        rich = 2,
        beautiful = 4,
        clever = 8,
        bold = 16,
        caring = 32,
        athletic = 64,
        stupid = 128,
        angry = 256,
        poor = 512,
        ugly = 1024,
        cowardly = 2048,
        selfish = 4096,
        fat = 8192,
        skinny = 16384
    }
}
