﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_1
{
    [Flags]
    public enum BrideProperties
    {   
        NULL = 0,
        kind = 1,
        rich = 2,
        beautiful = 4,
        clever = 8,
        stupid = 16,
        angry = 32,
        poor = 64,
        ugly = 128,
        breastSize_0 = 256,
        breastSize_1 = 512,
        breastSize_2 = 1024,
        breastSize_3 = 2048,
        breastSize_4 = 4096,
        breastSize_5 = 8192,
        athletic = 16384,
        skinny = 32768,
        fat = 65536
    }
}
