﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_1
{
    abstract class Person
    {
        #region Constants
        private const string DEFAULT_FIRST_NAME = "Undefined";
        private const string DEFAULT_SECOND_NAME = "Undefined";
        private const int DEFAULT_AGE = 18;
        #endregion

        #region Attributes
        private string firstName;
        private string secondName;
        private int age;
        #endregion

        #region Consructors
        protected Person() : this(DEFAULT_FIRST_NAME, DEFAULT_SECOND_NAME, DEFAULT_AGE) { }

        protected Person(string firstName, string secondName, int age)
        {
            this.firstName = firstName;
            this.secondName = secondName;
            Age = age;
        }
        #endregion

        #region Properties
        protected string FirstName
        {
            set => firstName = value;
            get => firstName;
        }

        protected string SecondName
        {
            set => secondName = value;
            get => secondName;
        }

        protected int Age
        {
            set
            {
                if(value >= 18)
                {
                    age = value;
                }
                else
                {
                    throw new ArgumentException("Age can't has value less 18");
                }
            }

            get => age;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return $"{firstName} {secondName} {age}";
        }
        #endregion
    }
}
