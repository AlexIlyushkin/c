﻿namespace Ex_1
{
    partial class MainMenu
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.openBrideMenuButton = new System.Windows.Forms.Button();
            this.openGroomMenuButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openBrideMenuButton
            // 
            this.openBrideMenuButton.AutoSize = true;
            this.openBrideMenuButton.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.openBrideMenuButton.Location = new System.Drawing.Point(348, 155);
            this.openBrideMenuButton.Name = "openBrideMenuButton";
            this.openBrideMenuButton.Size = new System.Drawing.Size(189, 45);
            this.openBrideMenuButton.TabIndex = 0;
            this.openBrideMenuButton.Text = "Найти невест";
            this.openBrideMenuButton.UseVisualStyleBackColor = true;
            this.openBrideMenuButton.Click += new System.EventHandler(this.openBrideMenuButton_Click);
            // 
            // openGroomMenuButton
            // 
            this.openGroomMenuButton.AutoSize = true;
            this.openGroomMenuButton.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Italic);
            this.openGroomMenuButton.Location = new System.Drawing.Point(348, 225);
            this.openGroomMenuButton.Name = "openGroomMenuButton";
            this.openGroomMenuButton.Size = new System.Drawing.Size(189, 45);
            this.openGroomMenuButton.TabIndex = 1;
            this.openGroomMenuButton.Text = "Найти женихов";
            this.openGroomMenuButton.UseVisualStyleBackColor = true;
            this.openGroomMenuButton.Click += new System.EventHandler(this.openGroomMenuButton_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 468);
            this.Controls.Add(this.openGroomMenuButton);
            this.Controls.Add(this.openBrideMenuButton);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximumSize = new System.Drawing.Size(900, 507);
            this.MinimumSize = new System.Drawing.Size(900, 507);
            this.Name = "MainMenu";
            this.Text = "Давай поженимся!";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button openBrideMenuButton;
        private System.Windows.Forms.Button openGroomMenuButton;
    }
}

