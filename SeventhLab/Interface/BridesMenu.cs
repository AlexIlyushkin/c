﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex_1
{
    public partial class BridesMenu : Form
    {
        #region Attributes
        private BrideManager brideManager;
        #endregion

        #region Constructors
        public BridesMenu()
        {
            InitializeComponent();            
        }
        #endregion

        #region Methods
        private void AddBrideToListView(Bride bride, ListView listView)
        {
            ListViewItem listViewItem = new ListViewItem(new string[] { bride.ToString(), bride.Properties.ToString() });
            listView.Items.Add(listViewItem);
        }

        private void AddBrideListToListView(List<Bride> brideList, ListView listView)
        {
            for (int i = 0; i < brideList.Count; i++)
            {
                AddBrideToListView(brideList[i], listView);
            }
        }
        #endregion

        #region Events
        private void BridesMenu_Load(object sender, EventArgs e)
        {            
            brideManager = new BrideManager();
            AddBrideListToListView(brideManager.AllBrides, listAllBrides);
            MessageBox.Show("Все персонажи вымышлены, любые сходства с реальностью - случайность.");
        }                

        private void buttonSelectBrides_Click(object sender, EventArgs e)
        {
            try
            {
                if(brideManager.PositiveProperties != 0 || brideManager.NegativeProperties != 0)
                {
                    brideManager.SelectBrides();
                    AddBrideListToListView(brideManager.BridesHavingOnlyPositiveProperties, listSelectedBrides);
                    AddBrideListToListView(brideManager.BridesHavingAllPositiveProperties, listSelectedBrides);
                    AddBrideListToListView(brideManager.BridesHavingSomePositiveProperties, listSelectedBrides);
                }
                else
                {
                    throw new FormatException("Вы не выбрали парметры отбора.");
                }
            }
            catch(FormatException exception)
            {
                MessageBox.Show(exception.Message);
            }
            
        }

        private void checkPositiveBreastSize_0_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPositiveBreastSize_0.Checked)
            {
                brideManager.AddPositiveProperty(BrideProperties.breastSize_0);

                if (checkNegativeBreastSize_0.Checked)
                {
                    checkNegativeBreastSize_0.Checked = false;
                }
            }
            else
            {
                brideManager.RemovePositiveProperty(BrideProperties.breastSize_0);
            }
        }

        private void checkNegativeBreastSize_0_CheckedChanged(object sender, EventArgs e)
        {
            if (checkNegativeBreastSize_0.Checked)
            {
                brideManager.AddNegativeProperty(BrideProperties.breastSize_0);

                if (checkPositiveBreastSize_0.Checked)
                {
                    checkPositiveBreastSize_0.Checked = false;
                }
            }
            else
            {
                brideManager.RemoveNegativeProperty(BrideProperties.breastSize_0);
            }
        }

        private void checkPositiveBreastSize_1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPositiveBreastSize_1.Checked)
            {
                brideManager.AddPositiveProperty(BrideProperties.breastSize_1);

                if (checkNegativeBreastSize_1.Checked)
                {
                    checkNegativeBreastSize_1.Checked = false;
                }
            }
            else
            {
                brideManager.RemovePositiveProperty(BrideProperties.breastSize_1);
            }
        }

        private void checkNegativeBreastSize_1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkNegativeBreastSize_1.Checked)
            {
                brideManager.AddNegativeProperty(BrideProperties.breastSize_1);

                if (checkPositiveBreastSize_1.Checked)
                {
                    checkPositiveBreastSize_1.Checked = false;
                }
            }
            else
            {
                brideManager.RemoveNegativeProperty(BrideProperties.breastSize_1);
            }
        }

        private void checkPositiveBreastSize_2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPositiveBreastSize_2.Checked)
            {
                brideManager.AddPositiveProperty(BrideProperties.breastSize_2);

                if (checkNegativeBreastSize_2.Checked)
                {
                    checkNegativeBreastSize_2.Checked = false;
                }
            }
            else
            {
                brideManager.RemovePositiveProperty(BrideProperties.breastSize_2);
            }
        }

        private void checkNegativeBreastSize_2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkNegativeBreastSize_2.Checked)
            {
                brideManager.AddNegativeProperty(BrideProperties.breastSize_2);

                if (checkPositiveBreastSize_2.Checked)
                {
                    checkPositiveBreastSize_2.Checked = false;
                }
            }
            else
            {
                brideManager.RemoveNegativeProperty(BrideProperties.breastSize_2);
            }
        }

        private void checkPositiveBreastSize_3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPositiveBreastSize_3.Checked)
            {
                brideManager.AddPositiveProperty(BrideProperties.breastSize_3);

                if (checkNegativeBreastSize_3.Checked)
                {
                    checkNegativeBreastSize_3.Checked = false;
                }
            }
            else
            {
                brideManager.RemovePositiveProperty(BrideProperties.breastSize_3);
            }
        }

        private void checkNegativeBreastSize_3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkNegativeBreastSize_3.Checked)
            {
                brideManager.AddNegativeProperty(BrideProperties.breastSize_3);

                if (checkPositiveBreastSize_3.Checked)
                {
                    checkPositiveBreastSize_3.Checked = false;
                }
            }
            else
            {
                brideManager.RemoveNegativeProperty(BrideProperties.breastSize_3);
            }
        }

        private void checkPositiveBreastSize_4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPositiveBreastSize_4.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.breastSize_4;

                if (checkNegativeBreastSize_4.Checked)
                {
                    checkNegativeBreastSize_4.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.breastSize_4;
            }
        }

        private void checkNegativeBreastSize_4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkNegativeBreastSize_4.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.breastSize_4;

                if (checkPositiveBreastSize_4.Checked)
                {
                    checkPositiveBreastSize_4.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.breastSize_4;
            }
        }

        private void checkPositiveBreastSize_5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPositiveBreastSize_5.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.breastSize_5;

                if (checkNegativeBreastSize_5.Checked)
                {
                    checkNegativeBreastSize_5.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.breastSize_5;
            }
        }

        private void checkNegativeBreastSize_5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkNegativeBreastSize_5.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.breastSize_5;

                if (checkPositiveBreastSize_5.Checked)
                {
                    checkPositiveBreastSize_5.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.breastSize_5;
            }
        }

        private void checkBeautifulPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBeautifulPositive.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.beautiful;

                if (checkBeautifulNegative.Checked)
                {
                    checkBeautifulNegative.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.beautiful;
            }
        }

        private void checkBeautifulNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBeautifulNegative.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.beautiful;

                if (checkBeautifulPositive.Checked)
                {
                    checkBeautifulPositive.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.beautiful;
            }
        }

        private void checkKindPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkKindPositive.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.kind;

                if (checkKindNegative.Checked)
                {
                    checkKindNegative.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.kind;
            }
        }

        private void checkKindNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkKindNegative.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.kind;

                if (checkKindPositive.Checked)
                {
                    checkKindPositive.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.kind;
            }
        }

        private void checkCleverPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCleverPositive.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.clever;

                if (checkCleverNegative.Checked)
                {
                    checkCleverNegative.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.clever;
            }
        }

        private void checkCleverNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCleverNegative.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.clever;

                if (checkCleverPositive.Checked)
                {
                    checkCleverPositive.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.clever;
            }
        }

        private void checkRichPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkRichPositive.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.rich;

                if (checkRichNegative.Checked)
                {
                    checkRichNegative.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.rich;
            }
        }

        private void checkRichNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkRichNegative.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.rich;

                if (checkRichPositive.Checked)
                {
                    checkRichPositive.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.rich;
            }
        }

        private void checkUglyPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkUglyPositive.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.ugly;

                if (checkUglyNegative.Checked)
                {
                    checkUglyNegative.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.ugly;
            }
        }

        private void checkUglyNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkUglyNegative.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.ugly;

                if (checkUglyPositive.Checked)
                {
                    checkUglyPositive.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.ugly;
            }
        }

        private void checkAngryPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAngryPositive.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.angry;

                if (checkAngryNegative.Checked)
                {
                    checkAngryNegative.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.angry;
            }
        }

        private void checkAngryNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAngryNegative.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.angry;

                if (checkAngryPositive.Checked)
                {
                    checkAngryPositive.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.angry;
            }
        }

        private void checkStupidPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkStupidPositive.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.stupid;

                if (checkStupidNegative.Checked)
                {
                    checkStupidNegative.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.stupid;
            }
        }

        private void checkStupidNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkStupidNegative.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.stupid;

                if (checkStupidPositive.Checked)
                {
                    checkStupidPositive.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.stupid;
            }
        }

        private void checkPoorPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPoorPositive.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.poor;

                if (checkPoorNegative.Checked)
                {
                    checkPoorNegative.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.poor;
            }
        }

        private void checkPoorNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPoorNegative.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.poor;

                if (checkPoorPositive.Checked)
                {
                    checkPoorPositive.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.poor;
            }
        }

        private void checkSkinnyPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkSkinnyPositive.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.skinny;

                if (checkSkinnyNegative.Checked)
                {
                    checkSkinnyNegative.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.skinny;
            }
        }

        private void checkSkinnyNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkSkinnyNegative.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.skinny;

                if (checkSkinnyPositive.Checked)
                {
                    checkSkinnyPositive.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.skinny;
            }
        }

        private void checkFatPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkFatPositive.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.fat;

                if (checkFatNegative.Checked)
                {
                    checkFatNegative.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.fat;
            }
        }

        private void checkFatNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkFatNegative.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.fat;

                if (checkFatPositive.Checked)
                {
                    checkFatPositive.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.fat;
            }
        }

        private void checkAthleticPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAthleticPositive.Checked)
            {
                brideManager.PositiveProperties |= BrideProperties.athletic;

                if (checkAthleticNegative.Checked)
                {
                    checkAthleticNegative.Checked = false;
                }
            }
            else
            {
                brideManager.PositiveProperties ^= BrideProperties.athletic;
            }
        }

        private void checkAthleticNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAthleticNegative.Checked)
            {
                brideManager.NegativeProperties |= BrideProperties.athletic;

                if (checkAthleticPositive.Checked)
                {
                    checkAthleticPositive.Checked = false;
                }
            }
            else
            {
                brideManager.NegativeProperties ^= BrideProperties.athletic;
            }
        }

        private void BridesMenu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }
        #endregion
    }
}
