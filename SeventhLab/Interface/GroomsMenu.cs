﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex_1
{
    public partial class GroomsMenu : Form
    {
        #region Attributes
        private GroomManager groomManager;
        #endregion

        #region Constructors
        public GroomsMenu()
        {
            InitializeComponent();
        }
        #endregion

        #region Methods
        private void AddGroomToListView(Groom groom, ListView listView)
        {
            ListViewItem listViewItem = new ListViewItem(new string[] { groom.ToString(), groom.Properties.ToString() });
            listView.Items.Add(listViewItem);
        }

        private void AddGroomListToListView(List<Groom> groomList, ListView listView)
        {
            for (int i = 0; i < groomList.Count; i++)
            {
                AddGroomToListView(groomList[i], listView);
            }
        }
        #endregion

        #region Events
        private void GroomsMenu_Load(object sender, EventArgs e)
        {
            groomManager = new GroomManager();
            AddGroomListToListView(groomManager.AllGrooms, listAllGrooms);
            MessageBox.Show("Все персонажи вымышлены, любые сходства с реальностью - случайность.");
        }        

        private void buttonSelectGrooms_Click(object sender, EventArgs e)
        {
            try
            {
                if (groomManager.PositiveProperties != 0 || groomManager.NegativeProperties != 0)
                {
                    groomManager.SelectGrooms();
                    AddGroomListToListView(groomManager.GroomsHavingOnlyPositiveProperties, listSelectedGrooms);
                    AddGroomListToListView(groomManager.GroomsHavingPositiveProperties, listSelectedGrooms);
                    AddGroomListToListView(groomManager.GroomsHavingSomePositiveProperties, listSelectedGrooms);                    
                }
                else
                {
                    throw new FormatException("Вы не выбрали парметры отбора");
                }
            }
            catch(FormatException exception)
            {
                MessageBox.Show(exception.Message);
            }
            
        }

        private void checkCleverPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCleverPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.clever;

                if (checkCleverNegative.Checked)
                {
                    checkCleverNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.clever;
            }
        }

        private void checkCleverNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCleverNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.clever;

                if (checkCleverPositive.Checked)
                {
                    checkCleverPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.clever;
            }
        }

        private void checkKindPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkKindPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.kind;

                if (checkKindNegative.Checked)
                {
                    checkKindNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.kind;
            }
        }

        private void checkKindNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkKindNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.kind;

                if (checkKindPositive.Checked)
                {
                    checkKindPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.kind;
            }
        }

        private void checkBeautifulPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBeautifulPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.beautiful;

                if (checkBeautifulNegative.Checked)
                {
                    checkBeautifulNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.beautiful;
            }
        }

        private void checkBeautifulNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBeautifulNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.beautiful;

                if (checkBeautifulPositive.Checked)
                {
                    checkBeautifulPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.beautiful;
            }
        }

        private void checkRichPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkRichPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.rich;

                if (checkRichNegative.Checked)
                {
                    checkRichNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.rich;
            }
        }

        private void checkRichNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkRichNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.rich;

                if (checkRichPositive.Checked)
                {
                    checkRichPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.rich;
            }
        }

        private void checkBoldPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoldPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.bold;

                if (checkBoldNegative.Checked)
                {
                    checkBoldNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.bold;
            }
        }

        private void checkBoldNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoldNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.bold;

                if (checkBoldPositive.Checked)
                {
                    checkBoldPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.bold;
            }
        }

        private void checkCaringPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCaringPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.caring;

                if (checkCaringNegative.Checked)
                {
                    checkCaringNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.caring;
            }
        }

        private void checkCaringNegative_CheckStateChanged(object sender, EventArgs e)
        {
            if (checkCaringNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.caring;

                if (checkCaringPositive.Checked)
                {
                    checkCaringPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.caring;
            }
        }

        private void checkStupidPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkStupidPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.stupid;

                if (checkStupidNegative.Checked)
                {
                    checkStupidNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.stupid;
            }
        }

        private void checkStupidNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkStupidNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.stupid;

                if (checkStupidPositive.Checked)
                {
                    checkStupidPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.stupid;
            }
        }

        private void checkAngryPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAngryPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.angry;

                if (checkAngryNegative.Checked)
                {
                    checkAngryNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.angry;
            }
        }

        private void checkAngryNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAngryNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.angry;

                if (checkAngryPositive.Checked)
                {
                    checkAngryPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.angry;
            }
        }

        private void checkUglyPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkUglyPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.ugly;

                if (checkUglyNegative.Checked)
                {
                    checkUglyNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.ugly;
            }
        }

        private void checkUglyNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkUglyNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.ugly;

                if (checkUglyPositive.Checked)
                {
                    checkUglyPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.ugly;
            }
        }

        private void checkPoorPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPoorPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.poor;

                if (checkPoorNegative.Checked)
                {
                    checkPoorNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.poor;
            }
        }

        private void checkPoorNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkPoorNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.poor;

                if (checkPoorPositive.Checked)
                {
                    checkPoorPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.poor;
            }
        }

        private void checkCowardlyPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCowardlyPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.cowardly;

                if (checkCowardlyNegative.Checked)
                {
                    checkCowardlyNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.cowardly;
            }
        }

        private void checkCowardlyNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCowardlyNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.cowardly;

                if (checkCowardlyPositive.Checked)
                {
                    checkCowardlyPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.cowardly;
            }
        }

        private void checkSelfishPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkSelfishPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.selfish;

                if (checkSelfishNegative.Checked)
                {
                    checkSelfishNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.selfish;
            }
        }

        private void checkSelfishNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkSelfishNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.selfish;

                if (checkSelfishPositive.Checked)
                {
                    checkSelfishPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.selfish;
            }
        }

        private void checkSkinnyPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkSkinnyPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.skinny;

                if (checkSkinnyNegative.Checked)
                {
                    checkSkinnyNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.skinny;
            }
        }

        private void checkSkinnyNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkSkinnyNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.skinny;

                if (checkSkinnyPositive.Checked)
                {
                    checkSkinnyPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.skinny;
            }
        }

        private void checkFatPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkFatPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.fat;

                if (checkFatNegative.Checked)
                {
                    checkFatNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.fat; 
            }
        }

        private void checkFatNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkFatNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.fat;

                if (checkFatPositive.Checked)
                {
                    checkFatPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.fat;
            }
        }

        private void checkAthleticPositive_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAthleticPositive.Checked)
            {
                groomManager.PositiveProperties |= GroomProperties.athletic;

                if (checkAthleticNegative.Checked)
                {
                    checkAthleticNegative.Checked = false;
                }
            }
            else
            {
                groomManager.PositiveProperties ^= GroomProperties.athletic;
            }
        }

        private void checkAthleticNegative_CheckedChanged(object sender, EventArgs e)
        {
            if (checkAthleticNegative.Checked)
            {
                groomManager.NegativeProperties |= GroomProperties.athletic;

                if (checkAthleticPositive.Checked)
                {
                    checkAthleticPositive.Checked = false;
                }
            }
            else
            {
                groomManager.NegativeProperties ^= GroomProperties.athletic;
            }
        }

        private void GroomsMenu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Show();
        }
        #endregion        
    }
}
