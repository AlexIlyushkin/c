﻿namespace Ex_1
{
    partial class BridesMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("");
            this.selectingParametersBox = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkNegativeBreastSize_5 = new System.Windows.Forms.CheckBox();
            this.checkNegativeBreastSize_0 = new System.Windows.Forms.CheckBox();
            this.checkNegativeBreastSize_1 = new System.Windows.Forms.CheckBox();
            this.checkNegativeBreastSize_2 = new System.Windows.Forms.CheckBox();
            this.checkNegativeBreastSize_4 = new System.Windows.Forms.CheckBox();
            this.checkNegativeBreastSize_3 = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkSkinnyNegative = new System.Windows.Forms.CheckBox();
            this.checkFatNegative = new System.Windows.Forms.CheckBox();
            this.checkAthleticNegative = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkPoorNegative = new System.Windows.Forms.CheckBox();
            this.checkRichNegative = new System.Windows.Forms.CheckBox();
            this.checkUglyNegative = new System.Windows.Forms.CheckBox();
            this.checkKindNegative = new System.Windows.Forms.CheckBox();
            this.checkCleverNegative = new System.Windows.Forms.CheckBox();
            this.checkBeautifulNegative = new System.Windows.Forms.CheckBox();
            this.checkAngryNegative = new System.Windows.Forms.CheckBox();
            this.checkStupidNegative = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkSkinnyPositive = new System.Windows.Forms.CheckBox();
            this.checkFatPositive = new System.Windows.Forms.CheckBox();
            this.checkAthleticPositive = new System.Windows.Forms.CheckBox();
            this.groupBreastSize = new System.Windows.Forms.GroupBox();
            this.checkPositiveBreastSize_5 = new System.Windows.Forms.CheckBox();
            this.checkPositiveBreastSize_0 = new System.Windows.Forms.CheckBox();
            this.checkPositiveBreastSize_1 = new System.Windows.Forms.CheckBox();
            this.checkPositiveBreastSize_3 = new System.Windows.Forms.CheckBox();
            this.checkPositiveBreastSize_4 = new System.Windows.Forms.CheckBox();
            this.checkPositiveBreastSize_2 = new System.Windows.Forms.CheckBox();
            this.buttonSelectBrides = new System.Windows.Forms.Button();
            this.positivePropertiesBox = new System.Windows.Forms.GroupBox();
            this.checkPoorPositive = new System.Windows.Forms.CheckBox();
            this.checkRichPositive = new System.Windows.Forms.CheckBox();
            this.checkUglyPositive = new System.Windows.Forms.CheckBox();
            this.checkKindPositive = new System.Windows.Forms.CheckBox();
            this.checkCleverPositive = new System.Windows.Forms.CheckBox();
            this.checkBeautifulPositive = new System.Windows.Forms.CheckBox();
            this.checkAngryPositive = new System.Windows.Forms.CheckBox();
            this.checkStupidPositive = new System.Windows.Forms.CheckBox();
            this.listAllBrides = new System.Windows.Forms.ListView();
            this.columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnProperties = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listSelectedBrides = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.labelAllBrides = new System.Windows.Forms.Label();
            this.labelSelectedBrides = new System.Windows.Forms.Label();
            this.buttonOpenAddBrideMenu = new System.Windows.Forms.Button();
            this.buttonMainMenu = new System.Windows.Forms.Button();
            this.selectingParametersBox.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBreastSize.SuspendLayout();
            this.positivePropertiesBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // selectingParametersBox
            // 
            this.selectingParametersBox.Controls.Add(this.groupBox4);
            this.selectingParametersBox.Controls.Add(this.groupBox3);
            this.selectingParametersBox.Controls.Add(this.groupBox2);
            this.selectingParametersBox.Controls.Add(this.groupBox1);
            this.selectingParametersBox.Controls.Add(this.groupBreastSize);
            this.selectingParametersBox.Controls.Add(this.buttonSelectBrides);
            this.selectingParametersBox.Controls.Add(this.positivePropertiesBox);
            this.selectingParametersBox.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Italic);
            this.selectingParametersBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.selectingParametersBox.Location = new System.Drawing.Point(2, 12);
            this.selectingParametersBox.Name = "selectingParametersBox";
            this.selectingParametersBox.Size = new System.Drawing.Size(206, 560);
            this.selectingParametersBox.TabIndex = 0;
            this.selectingParametersBox.TabStop = false;
            this.selectingParametersBox.Text = "Параметры отбора";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkNegativeBreastSize_5);
            this.groupBox4.Controls.Add(this.checkNegativeBreastSize_0);
            this.groupBox4.Controls.Add(this.checkNegativeBreastSize_1);
            this.groupBox4.Controls.Add(this.checkNegativeBreastSize_2);
            this.groupBox4.Controls.Add(this.checkNegativeBreastSize_4);
            this.groupBox4.Controls.Add(this.checkNegativeBreastSize_3);
            this.groupBox4.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.groupBox4.Location = new System.Drawing.Point(6, 447);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(189, 52);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Нежелательный размер";
            // 
            // checkNegativeBreastSize_5
            // 
            this.checkNegativeBreastSize_5.AutoSize = true;
            this.checkNegativeBreastSize_5.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkNegativeBreastSize_5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkNegativeBreastSize_5.Location = new System.Drawing.Point(128, 17);
            this.checkNegativeBreastSize_5.Name = "checkNegativeBreastSize_5";
            this.checkNegativeBreastSize_5.Size = new System.Drawing.Size(17, 33);
            this.checkNegativeBreastSize_5.TabIndex = 13;
            this.checkNegativeBreastSize_5.Text = "5";
            this.checkNegativeBreastSize_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkNegativeBreastSize_5.UseVisualStyleBackColor = true;
            this.checkNegativeBreastSize_5.CheckedChanged += new System.EventHandler(this.checkNegativeBreastSize_5_CheckedChanged);
            // 
            // checkNegativeBreastSize_0
            // 
            this.checkNegativeBreastSize_0.AutoSize = true;
            this.checkNegativeBreastSize_0.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkNegativeBreastSize_0.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkNegativeBreastSize_0.Location = new System.Drawing.Point(13, 17);
            this.checkNegativeBreastSize_0.Name = "checkNegativeBreastSize_0";
            this.checkNegativeBreastSize_0.Size = new System.Drawing.Size(17, 33);
            this.checkNegativeBreastSize_0.TabIndex = 8;
            this.checkNegativeBreastSize_0.Text = "0";
            this.checkNegativeBreastSize_0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkNegativeBreastSize_0.UseVisualStyleBackColor = true;
            this.checkNegativeBreastSize_0.CheckedChanged += new System.EventHandler(this.checkNegativeBreastSize_0_CheckedChanged);
            // 
            // checkNegativeBreastSize_1
            // 
            this.checkNegativeBreastSize_1.AutoSize = true;
            this.checkNegativeBreastSize_1.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkNegativeBreastSize_1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkNegativeBreastSize_1.Location = new System.Drawing.Point(36, 17);
            this.checkNegativeBreastSize_1.Name = "checkNegativeBreastSize_1";
            this.checkNegativeBreastSize_1.Size = new System.Drawing.Size(17, 33);
            this.checkNegativeBreastSize_1.TabIndex = 9;
            this.checkNegativeBreastSize_1.Text = "1";
            this.checkNegativeBreastSize_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkNegativeBreastSize_1.UseVisualStyleBackColor = true;
            this.checkNegativeBreastSize_1.CheckedChanged += new System.EventHandler(this.checkNegativeBreastSize_1_CheckedChanged);
            // 
            // checkNegativeBreastSize_2
            // 
            this.checkNegativeBreastSize_2.AutoSize = true;
            this.checkNegativeBreastSize_2.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkNegativeBreastSize_2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkNegativeBreastSize_2.Location = new System.Drawing.Point(58, 17);
            this.checkNegativeBreastSize_2.Name = "checkNegativeBreastSize_2";
            this.checkNegativeBreastSize_2.Size = new System.Drawing.Size(17, 33);
            this.checkNegativeBreastSize_2.TabIndex = 10;
            this.checkNegativeBreastSize_2.Text = "2";
            this.checkNegativeBreastSize_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkNegativeBreastSize_2.UseVisualStyleBackColor = true;
            this.checkNegativeBreastSize_2.CheckedChanged += new System.EventHandler(this.checkNegativeBreastSize_2_CheckedChanged);
            // 
            // checkNegativeBreastSize_4
            // 
            this.checkNegativeBreastSize_4.AutoSize = true;
            this.checkNegativeBreastSize_4.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkNegativeBreastSize_4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkNegativeBreastSize_4.Location = new System.Drawing.Point(105, 17);
            this.checkNegativeBreastSize_4.Name = "checkNegativeBreastSize_4";
            this.checkNegativeBreastSize_4.Size = new System.Drawing.Size(17, 33);
            this.checkNegativeBreastSize_4.TabIndex = 12;
            this.checkNegativeBreastSize_4.Text = "4";
            this.checkNegativeBreastSize_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkNegativeBreastSize_4.UseVisualStyleBackColor = true;
            this.checkNegativeBreastSize_4.CheckedChanged += new System.EventHandler(this.checkNegativeBreastSize_4_CheckedChanged);
            // 
            // checkNegativeBreastSize_3
            // 
            this.checkNegativeBreastSize_3.AutoSize = true;
            this.checkNegativeBreastSize_3.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkNegativeBreastSize_3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkNegativeBreastSize_3.Location = new System.Drawing.Point(81, 17);
            this.checkNegativeBreastSize_3.Name = "checkNegativeBreastSize_3";
            this.checkNegativeBreastSize_3.Size = new System.Drawing.Size(17, 33);
            this.checkNegativeBreastSize_3.TabIndex = 11;
            this.checkNegativeBreastSize_3.Text = "3";
            this.checkNegativeBreastSize_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkNegativeBreastSize_3.UseVisualStyleBackColor = true;
            this.checkNegativeBreastSize_3.CheckedChanged += new System.EventHandler(this.checkNegativeBreastSize_3_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkSkinnyNegative);
            this.groupBox3.Controls.Add(this.checkFatNegative);
            this.groupBox3.Controls.Add(this.checkAthleticNegative);
            this.groupBox3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(6, 308);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(189, 72);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Нежелательная фигура";
            // 
            // checkSkinnyNegative
            // 
            this.checkSkinnyNegative.AutoSize = true;
            this.checkSkinnyNegative.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.checkSkinnyNegative.Location = new System.Drawing.Point(10, 14);
            this.checkSkinnyNegative.Name = "checkSkinnyNegative";
            this.checkSkinnyNegative.Size = new System.Drawing.Size(83, 20);
            this.checkSkinnyNegative.TabIndex = 5;
            this.checkSkinnyNegative.Text = "Худышка";
            this.checkSkinnyNegative.UseVisualStyleBackColor = true;
            this.checkSkinnyNegative.CheckedChanged += new System.EventHandler(this.checkSkinnyNegative_CheckedChanged);
            // 
            // checkFatNegative
            // 
            this.checkFatNegative.AutoSize = true;
            this.checkFatNegative.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.checkFatNegative.Location = new System.Drawing.Point(10, 32);
            this.checkFatNegative.Name = "checkFatNegative";
            this.checkFatNegative.Size = new System.Drawing.Size(94, 20);
            this.checkFatNegative.TabIndex = 8;
            this.checkFatNegative.Text = "Толстушка";
            this.checkFatNegative.UseVisualStyleBackColor = true;
            this.checkFatNegative.CheckedChanged += new System.EventHandler(this.checkFatNegative_CheckedChanged);
            // 
            // checkAthleticNegative
            // 
            this.checkAthleticNegative.AutoSize = true;
            this.checkAthleticNegative.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.checkAthleticNegative.Location = new System.Drawing.Point(10, 49);
            this.checkAthleticNegative.Name = "checkAthleticNegative";
            this.checkAthleticNegative.Size = new System.Drawing.Size(102, 20);
            this.checkAthleticNegative.TabIndex = 4;
            this.checkAthleticNegative.Text = "Фитоняшка";
            this.checkAthleticNegative.UseVisualStyleBackColor = true;
            this.checkAthleticNegative.CheckedChanged += new System.EventHandler(this.checkAthleticNegative_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkPoorNegative);
            this.groupBox2.Controls.Add(this.checkRichNegative);
            this.groupBox2.Controls.Add(this.checkUglyNegative);
            this.groupBox2.Controls.Add(this.checkKindNegative);
            this.groupBox2.Controls.Add(this.checkCleverNegative);
            this.groupBox2.Controls.Add(this.checkBeautifulNegative);
            this.groupBox2.Controls.Add(this.checkAngryNegative);
            this.groupBox2.Controls.Add(this.checkStupidNegative);
            this.groupBox2.Location = new System.Drawing.Point(6, 126);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(192, 98);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Нежелательные черты";
            // 
            // checkPoorNegative
            // 
            this.checkPoorNegative.AutoSize = true;
            this.checkPoorNegative.Location = new System.Drawing.Point(102, 75);
            this.checkPoorNegative.Name = "checkPoorNegative";
            this.checkPoorNegative.Size = new System.Drawing.Size(74, 21);
            this.checkPoorNegative.TabIndex = 3;
            this.checkPoorNegative.Text = "Бедная";
            this.checkPoorNegative.UseVisualStyleBackColor = true;
            this.checkPoorNegative.CheckedChanged += new System.EventHandler(this.checkPoorNegative_CheckedChanged);
            // 
            // checkRichNegative
            // 
            this.checkRichNegative.AutoSize = true;
            this.checkRichNegative.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Italic);
            this.checkRichNegative.Location = new System.Drawing.Point(6, 75);
            this.checkRichNegative.Name = "checkRichNegative";
            this.checkRichNegative.Size = new System.Drawing.Size(83, 21);
            this.checkRichNegative.TabIndex = 2;
            this.checkRichNegative.Text = "Богатая";
            this.checkRichNegative.UseVisualStyleBackColor = true;
            this.checkRichNegative.CheckedChanged += new System.EventHandler(this.checkRichNegative_CheckedChanged);
            // 
            // checkUglyNegative
            // 
            this.checkUglyNegative.AutoSize = true;
            this.checkUglyNegative.Location = new System.Drawing.Point(102, 22);
            this.checkUglyNegative.Name = "checkUglyNegative";
            this.checkUglyNegative.Size = new System.Drawing.Size(87, 21);
            this.checkUglyNegative.TabIndex = 2;
            this.checkUglyNegative.Text = "На разок";
            this.checkUglyNegative.UseVisualStyleBackColor = true;
            this.checkUglyNegative.CheckedChanged += new System.EventHandler(this.checkUglyNegative_CheckedChanged);
            // 
            // checkKindNegative
            // 
            this.checkKindNegative.AutoSize = true;
            this.checkKindNegative.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Italic);
            this.checkKindNegative.Location = new System.Drawing.Point(6, 40);
            this.checkKindNegative.Name = "checkKindNegative";
            this.checkKindNegative.Size = new System.Drawing.Size(76, 21);
            this.checkKindNegative.TabIndex = 1;
            this.checkKindNegative.Text = "Добрая";
            this.checkKindNegative.UseVisualStyleBackColor = true;
            this.checkKindNegative.CheckedChanged += new System.EventHandler(this.checkKindNegative_CheckedChanged);
            // 
            // checkCleverNegative
            // 
            this.checkCleverNegative.AutoSize = true;
            this.checkCleverNegative.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Italic);
            this.checkCleverNegative.Location = new System.Drawing.Point(6, 58);
            this.checkCleverNegative.Name = "checkCleverNegative";
            this.checkCleverNegative.Size = new System.Drawing.Size(69, 21);
            this.checkCleverNegative.TabIndex = 3;
            this.checkCleverNegative.Text = "Умная";
            this.checkCleverNegative.UseVisualStyleBackColor = true;
            this.checkCleverNegative.CheckedChanged += new System.EventHandler(this.checkCleverNegative_CheckedChanged);
            // 
            // checkBeautifulNegative
            // 
            this.checkBeautifulNegative.AutoSize = true;
            this.checkBeautifulNegative.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Italic);
            this.checkBeautifulNegative.Location = new System.Drawing.Point(6, 22);
            this.checkBeautifulNegative.Name = "checkBeautifulNegative";
            this.checkBeautifulNegative.Size = new System.Drawing.Size(90, 21);
            this.checkBeautifulNegative.TabIndex = 0;
            this.checkBeautifulNegative.Text = "Красивая";
            this.checkBeautifulNegative.UseVisualStyleBackColor = true;
            this.checkBeautifulNegative.CheckedChanged += new System.EventHandler(this.checkBeautifulNegative_CheckedChanged);
            // 
            // checkAngryNegative
            // 
            this.checkAngryNegative.AutoSize = true;
            this.checkAngryNegative.Location = new System.Drawing.Point(102, 40);
            this.checkAngryNegative.Name = "checkAngryNegative";
            this.checkAngryNegative.Size = new System.Drawing.Size(77, 21);
            this.checkAngryNegative.TabIndex = 1;
            this.checkAngryNegative.Text = "Стерва";
            this.checkAngryNegative.UseVisualStyleBackColor = true;
            this.checkAngryNegative.CheckedChanged += new System.EventHandler(this.checkAngryNegative_CheckedChanged);
            // 
            // checkStupidNegative
            // 
            this.checkStupidNegative.AutoSize = true;
            this.checkStupidNegative.Location = new System.Drawing.Point(102, 58);
            this.checkStupidNegative.Name = "checkStupidNegative";
            this.checkStupidNegative.Size = new System.Drawing.Size(72, 21);
            this.checkStupidNegative.TabIndex = 0;
            this.checkStupidNegative.Text = "Глупая";
            this.checkStupidNegative.UseVisualStyleBackColor = true;
            this.checkStupidNegative.CheckedChanged += new System.EventHandler(this.checkStupidNegative_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkSkinnyPositive);
            this.groupBox1.Controls.Add(this.checkFatPositive);
            this.groupBox1.Controls.Add(this.checkAthleticPositive);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(6, 230);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(189, 72);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Желательная фигура";
            // 
            // checkSkinnyPositive
            // 
            this.checkSkinnyPositive.AutoSize = true;
            this.checkSkinnyPositive.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.checkSkinnyPositive.Location = new System.Drawing.Point(10, 14);
            this.checkSkinnyPositive.Name = "checkSkinnyPositive";
            this.checkSkinnyPositive.Size = new System.Drawing.Size(83, 20);
            this.checkSkinnyPositive.TabIndex = 5;
            this.checkSkinnyPositive.Text = "Худышка";
            this.checkSkinnyPositive.UseVisualStyleBackColor = true;
            this.checkSkinnyPositive.CheckedChanged += new System.EventHandler(this.checkSkinnyPositive_CheckedChanged);
            // 
            // checkFatPositive
            // 
            this.checkFatPositive.AutoSize = true;
            this.checkFatPositive.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.checkFatPositive.Location = new System.Drawing.Point(10, 32);
            this.checkFatPositive.Name = "checkFatPositive";
            this.checkFatPositive.Size = new System.Drawing.Size(94, 20);
            this.checkFatPositive.TabIndex = 8;
            this.checkFatPositive.Text = "Толстушка";
            this.checkFatPositive.UseVisualStyleBackColor = true;
            this.checkFatPositive.CheckedChanged += new System.EventHandler(this.checkFatPositive_CheckedChanged);
            // 
            // checkAthleticPositive
            // 
            this.checkAthleticPositive.AutoSize = true;
            this.checkAthleticPositive.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.checkAthleticPositive.Location = new System.Drawing.Point(10, 49);
            this.checkAthleticPositive.Name = "checkAthleticPositive";
            this.checkAthleticPositive.Size = new System.Drawing.Size(102, 20);
            this.checkAthleticPositive.TabIndex = 4;
            this.checkAthleticPositive.Text = "Фитоняшка";
            this.checkAthleticPositive.UseVisualStyleBackColor = true;
            this.checkAthleticPositive.CheckedChanged += new System.EventHandler(this.checkAthleticPositive_CheckedChanged);
            // 
            // groupBreastSize
            // 
            this.groupBreastSize.Controls.Add(this.checkPositiveBreastSize_5);
            this.groupBreastSize.Controls.Add(this.checkPositiveBreastSize_0);
            this.groupBreastSize.Controls.Add(this.checkPositiveBreastSize_1);
            this.groupBreastSize.Controls.Add(this.checkPositiveBreastSize_3);
            this.groupBreastSize.Controls.Add(this.checkPositiveBreastSize_4);
            this.groupBreastSize.Controls.Add(this.checkPositiveBreastSize_2);
            this.groupBreastSize.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.groupBreastSize.Location = new System.Drawing.Point(6, 386);
            this.groupBreastSize.Name = "groupBreastSize";
            this.groupBreastSize.Size = new System.Drawing.Size(189, 55);
            this.groupBreastSize.TabIndex = 5;
            this.groupBreastSize.TabStop = false;
            this.groupBreastSize.Text = "Желательный размер груди";
            // 
            // checkPositiveBreastSize_5
            // 
            this.checkPositiveBreastSize_5.AutoSize = true;
            this.checkPositiveBreastSize_5.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkPositiveBreastSize_5.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkPositiveBreastSize_5.Location = new System.Drawing.Point(128, 17);
            this.checkPositiveBreastSize_5.Name = "checkPositiveBreastSize_5";
            this.checkPositiveBreastSize_5.Size = new System.Drawing.Size(17, 33);
            this.checkPositiveBreastSize_5.TabIndex = 13;
            this.checkPositiveBreastSize_5.Text = "5";
            this.checkPositiveBreastSize_5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkPositiveBreastSize_5.UseVisualStyleBackColor = true;
            this.checkPositiveBreastSize_5.CheckedChanged += new System.EventHandler(this.checkPositiveBreastSize_5_CheckedChanged);
            // 
            // checkPositiveBreastSize_0
            // 
            this.checkPositiveBreastSize_0.AutoSize = true;
            this.checkPositiveBreastSize_0.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkPositiveBreastSize_0.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkPositiveBreastSize_0.Location = new System.Drawing.Point(13, 17);
            this.checkPositiveBreastSize_0.Name = "checkPositiveBreastSize_0";
            this.checkPositiveBreastSize_0.Size = new System.Drawing.Size(17, 33);
            this.checkPositiveBreastSize_0.TabIndex = 8;
            this.checkPositiveBreastSize_0.Text = "0";
            this.checkPositiveBreastSize_0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkPositiveBreastSize_0.UseVisualStyleBackColor = true;
            this.checkPositiveBreastSize_0.CheckedChanged += new System.EventHandler(this.checkPositiveBreastSize_0_CheckedChanged);
            // 
            // checkPositiveBreastSize_1
            // 
            this.checkPositiveBreastSize_1.AutoSize = true;
            this.checkPositiveBreastSize_1.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkPositiveBreastSize_1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkPositiveBreastSize_1.Location = new System.Drawing.Point(36, 17);
            this.checkPositiveBreastSize_1.Name = "checkPositiveBreastSize_1";
            this.checkPositiveBreastSize_1.Size = new System.Drawing.Size(17, 33);
            this.checkPositiveBreastSize_1.TabIndex = 9;
            this.checkPositiveBreastSize_1.Text = "1";
            this.checkPositiveBreastSize_1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkPositiveBreastSize_1.UseVisualStyleBackColor = true;
            this.checkPositiveBreastSize_1.CheckedChanged += new System.EventHandler(this.checkPositiveBreastSize_1_CheckedChanged);
            // 
            // checkPositiveBreastSize_3
            // 
            this.checkPositiveBreastSize_3.AutoSize = true;
            this.checkPositiveBreastSize_3.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkPositiveBreastSize_3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkPositiveBreastSize_3.Location = new System.Drawing.Point(82, 17);
            this.checkPositiveBreastSize_3.Name = "checkPositiveBreastSize_3";
            this.checkPositiveBreastSize_3.Size = new System.Drawing.Size(17, 33);
            this.checkPositiveBreastSize_3.TabIndex = 11;
            this.checkPositiveBreastSize_3.Text = "3";
            this.checkPositiveBreastSize_3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkPositiveBreastSize_3.UseVisualStyleBackColor = true;
            this.checkPositiveBreastSize_3.CheckedChanged += new System.EventHandler(this.checkPositiveBreastSize_3_CheckedChanged);
            // 
            // checkPositiveBreastSize_4
            // 
            this.checkPositiveBreastSize_4.AutoSize = true;
            this.checkPositiveBreastSize_4.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkPositiveBreastSize_4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkPositiveBreastSize_4.Location = new System.Drawing.Point(105, 17);
            this.checkPositiveBreastSize_4.Name = "checkPositiveBreastSize_4";
            this.checkPositiveBreastSize_4.Size = new System.Drawing.Size(17, 33);
            this.checkPositiveBreastSize_4.TabIndex = 12;
            this.checkPositiveBreastSize_4.Text = "4";
            this.checkPositiveBreastSize_4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkPositiveBreastSize_4.UseVisualStyleBackColor = true;
            this.checkPositiveBreastSize_4.CheckedChanged += new System.EventHandler(this.checkPositiveBreastSize_4_CheckedChanged);
            // 
            // checkPositiveBreastSize_2
            // 
            this.checkPositiveBreastSize_2.AutoSize = true;
            this.checkPositiveBreastSize_2.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.checkPositiveBreastSize_2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkPositiveBreastSize_2.Location = new System.Drawing.Point(59, 17);
            this.checkPositiveBreastSize_2.Name = "checkPositiveBreastSize_2";
            this.checkPositiveBreastSize_2.Size = new System.Drawing.Size(17, 33);
            this.checkPositiveBreastSize_2.TabIndex = 10;
            this.checkPositiveBreastSize_2.Text = "2";
            this.checkPositiveBreastSize_2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkPositiveBreastSize_2.UseVisualStyleBackColor = true;
            this.checkPositiveBreastSize_2.CheckedChanged += new System.EventHandler(this.checkPositiveBreastSize_2_CheckedChanged);
            // 
            // buttonSelectBrides
            // 
            this.buttonSelectBrides.Location = new System.Drawing.Point(28, 519);
            this.buttonSelectBrides.Name = "buttonSelectBrides";
            this.buttonSelectBrides.Size = new System.Drawing.Size(161, 35);
            this.buttonSelectBrides.TabIndex = 4;
            this.buttonSelectBrides.Text = "Найти кандидаток";
            this.buttonSelectBrides.UseVisualStyleBackColor = true;
            this.buttonSelectBrides.Click += new System.EventHandler(this.buttonSelectBrides_Click);
            // 
            // positivePropertiesBox
            // 
            this.positivePropertiesBox.Controls.Add(this.checkPoorPositive);
            this.positivePropertiesBox.Controls.Add(this.checkRichPositive);
            this.positivePropertiesBox.Controls.Add(this.checkUglyPositive);
            this.positivePropertiesBox.Controls.Add(this.checkKindPositive);
            this.positivePropertiesBox.Controls.Add(this.checkCleverPositive);
            this.positivePropertiesBox.Controls.Add(this.checkBeautifulPositive);
            this.positivePropertiesBox.Controls.Add(this.checkAngryPositive);
            this.positivePropertiesBox.Controls.Add(this.checkStupidPositive);
            this.positivePropertiesBox.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.positivePropertiesBox.Location = new System.Drawing.Point(6, 22);
            this.positivePropertiesBox.Name = "positivePropertiesBox";
            this.positivePropertiesBox.Size = new System.Drawing.Size(192, 98);
            this.positivePropertiesBox.TabIndex = 4;
            this.positivePropertiesBox.TabStop = false;
            this.positivePropertiesBox.Text = "Желательные черты";
            // 
            // checkPoorPositive
            // 
            this.checkPoorPositive.AutoSize = true;
            this.checkPoorPositive.Location = new System.Drawing.Point(102, 75);
            this.checkPoorPositive.Name = "checkPoorPositive";
            this.checkPoorPositive.Size = new System.Drawing.Size(74, 21);
            this.checkPoorPositive.TabIndex = 3;
            this.checkPoorPositive.Text = "Бедная";
            this.checkPoorPositive.UseVisualStyleBackColor = true;
            this.checkPoorPositive.CheckedChanged += new System.EventHandler(this.checkPoorPositive_CheckedChanged);
            // 
            // checkRichPositive
            // 
            this.checkRichPositive.AutoSize = true;
            this.checkRichPositive.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Italic);
            this.checkRichPositive.Location = new System.Drawing.Point(6, 75);
            this.checkRichPositive.Name = "checkRichPositive";
            this.checkRichPositive.Size = new System.Drawing.Size(83, 21);
            this.checkRichPositive.TabIndex = 2;
            this.checkRichPositive.Text = "Богатая";
            this.checkRichPositive.UseVisualStyleBackColor = true;
            this.checkRichPositive.CheckedChanged += new System.EventHandler(this.checkRichPositive_CheckedChanged);
            // 
            // checkUglyPositive
            // 
            this.checkUglyPositive.AutoSize = true;
            this.checkUglyPositive.Location = new System.Drawing.Point(102, 22);
            this.checkUglyPositive.Name = "checkUglyPositive";
            this.checkUglyPositive.Size = new System.Drawing.Size(87, 21);
            this.checkUglyPositive.TabIndex = 2;
            this.checkUglyPositive.Text = "На разок";
            this.checkUglyPositive.UseVisualStyleBackColor = true;
            this.checkUglyPositive.CheckedChanged += new System.EventHandler(this.checkUglyPositive_CheckedChanged);
            // 
            // checkKindPositive
            // 
            this.checkKindPositive.AutoSize = true;
            this.checkKindPositive.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Italic);
            this.checkKindPositive.Location = new System.Drawing.Point(6, 40);
            this.checkKindPositive.Name = "checkKindPositive";
            this.checkKindPositive.Size = new System.Drawing.Size(76, 21);
            this.checkKindPositive.TabIndex = 1;
            this.checkKindPositive.Text = "Добрая";
            this.checkKindPositive.UseVisualStyleBackColor = true;
            this.checkKindPositive.CheckedChanged += new System.EventHandler(this.checkKindPositive_CheckedChanged);
            // 
            // checkCleverPositive
            // 
            this.checkCleverPositive.AutoSize = true;
            this.checkCleverPositive.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Italic);
            this.checkCleverPositive.Location = new System.Drawing.Point(6, 58);
            this.checkCleverPositive.Name = "checkCleverPositive";
            this.checkCleverPositive.Size = new System.Drawing.Size(69, 21);
            this.checkCleverPositive.TabIndex = 3;
            this.checkCleverPositive.Text = "Умная";
            this.checkCleverPositive.UseVisualStyleBackColor = true;
            this.checkCleverPositive.CheckedChanged += new System.EventHandler(this.checkCleverPositive_CheckedChanged);
            // 
            // checkBeautifulPositive
            // 
            this.checkBeautifulPositive.AutoSize = true;
            this.checkBeautifulPositive.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Italic);
            this.checkBeautifulPositive.Location = new System.Drawing.Point(6, 22);
            this.checkBeautifulPositive.Name = "checkBeautifulPositive";
            this.checkBeautifulPositive.Size = new System.Drawing.Size(90, 21);
            this.checkBeautifulPositive.TabIndex = 0;
            this.checkBeautifulPositive.Text = "Красивая";
            this.checkBeautifulPositive.UseVisualStyleBackColor = true;
            this.checkBeautifulPositive.CheckedChanged += new System.EventHandler(this.checkBeautifulPositive_CheckedChanged);
            // 
            // checkAngryPositive
            // 
            this.checkAngryPositive.AutoSize = true;
            this.checkAngryPositive.Location = new System.Drawing.Point(102, 40);
            this.checkAngryPositive.Name = "checkAngryPositive";
            this.checkAngryPositive.Size = new System.Drawing.Size(77, 21);
            this.checkAngryPositive.TabIndex = 1;
            this.checkAngryPositive.Text = "Стерва";
            this.checkAngryPositive.UseVisualStyleBackColor = true;
            this.checkAngryPositive.CheckedChanged += new System.EventHandler(this.checkAngryPositive_CheckedChanged);
            // 
            // checkStupidPositive
            // 
            this.checkStupidPositive.AutoSize = true;
            this.checkStupidPositive.Location = new System.Drawing.Point(102, 58);
            this.checkStupidPositive.Name = "checkStupidPositive";
            this.checkStupidPositive.Size = new System.Drawing.Size(72, 21);
            this.checkStupidPositive.TabIndex = 0;
            this.checkStupidPositive.Text = "Глупая";
            this.checkStupidPositive.UseVisualStyleBackColor = true;
            this.checkStupidPositive.CheckedChanged += new System.EventHandler(this.checkStupidPositive_CheckedChanged);
            // 
            // listAllBrides
            // 
            this.listAllBrides.BackColor = System.Drawing.SystemColors.Info;
            this.listAllBrides.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnName,
            this.columnProperties});
            this.listAllBrides.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listAllBrides.ForeColor = System.Drawing.SystemColors.WindowText;
            this.listAllBrides.GridLines = true;
            this.listAllBrides.HideSelection = false;
            this.listAllBrides.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.listAllBrides.Location = new System.Drawing.Point(225, 34);
            this.listAllBrides.Name = "listAllBrides";
            this.listAllBrides.Size = new System.Drawing.Size(315, 620);
            this.listAllBrides.TabIndex = 2;
            this.listAllBrides.UseCompatibleStateImageBehavior = false;
            this.listAllBrides.View = System.Windows.Forms.View.Details;
            // 
            // columnName
            // 
            this.columnName.Text = "Имя";
            this.columnName.Width = 120;
            // 
            // columnProperties
            // 
            this.columnProperties.Text = "Черты";
            this.columnProperties.Width = 600;
            // 
            // listSelectedBrides
            // 
            this.listSelectedBrides.BackColor = System.Drawing.SystemColors.Info;
            this.listSelectedBrides.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listSelectedBrides.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listSelectedBrides.ForeColor = System.Drawing.SystemColors.WindowText;
            this.listSelectedBrides.GridLines = true;
            this.listSelectedBrides.HideSelection = false;
            this.listSelectedBrides.Location = new System.Drawing.Point(557, 34);
            this.listSelectedBrides.Name = "listSelectedBrides";
            this.listSelectedBrides.Size = new System.Drawing.Size(315, 620);
            this.listSelectedBrides.TabIndex = 3;
            this.listSelectedBrides.UseCompatibleStateImageBehavior = false;
            this.listSelectedBrides.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Имя";
            this.columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Черты";
            this.columnHeader2.Width = 600;
            // 
            // labelAllBrides
            // 
            this.labelAllBrides.AutoSize = true;
            this.labelAllBrides.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAllBrides.Location = new System.Drawing.Point(232, 14);
            this.labelAllBrides.Name = "labelAllBrides";
            this.labelAllBrides.Size = new System.Drawing.Size(154, 17);
            this.labelAllBrides.TabIndex = 4;
            this.labelAllBrides.Text = "Общий список невест";
            // 
            // labelSelectedBrides
            // 
            this.labelSelectedBrides.AutoSize = true;
            this.labelSelectedBrides.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSelectedBrides.Location = new System.Drawing.Point(563, 14);
            this.labelSelectedBrides.Name = "labelSelectedBrides";
            this.labelSelectedBrides.Size = new System.Drawing.Size(238, 17);
            this.labelSelectedBrides.TabIndex = 5;
            this.labelSelectedBrides.Text = "Список предпочтительных невест";
            // 
            // buttonOpenAddBrideMenu
            // 
            this.buttonOpenAddBrideMenu.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonOpenAddBrideMenu.Location = new System.Drawing.Point(30, 578);
            this.buttonOpenAddBrideMenu.Name = "buttonOpenAddBrideMenu";
            this.buttonOpenAddBrideMenu.Size = new System.Drawing.Size(161, 35);
            this.buttonOpenAddBrideMenu.TabIndex = 6;
            this.buttonOpenAddBrideMenu.Text = "Добавить невесту";
            this.buttonOpenAddBrideMenu.UseVisualStyleBackColor = true;
            // 
            // buttonMainMenu
            // 
            this.buttonMainMenu.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonMainMenu.Location = new System.Drawing.Point(30, 619);
            this.buttonMainMenu.Name = "buttonMainMenu";
            this.buttonMainMenu.Size = new System.Drawing.Size(161, 35);
            this.buttonMainMenu.TabIndex = 7;
            this.buttonMainMenu.Text = "Главное меню";
            this.buttonMainMenu.UseVisualStyleBackColor = true;
            // 
            // BridesMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(884, 666);
            this.Controls.Add(this.buttonMainMenu);
            this.Controls.Add(this.buttonOpenAddBrideMenu);
            this.Controls.Add(this.labelSelectedBrides);
            this.Controls.Add(this.labelAllBrides);
            this.Controls.Add(this.listSelectedBrides);
            this.Controls.Add(this.listAllBrides);
            this.Controls.Add(this.selectingParametersBox);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximumSize = new System.Drawing.Size(900, 705);
            this.MinimumSize = new System.Drawing.Size(900, 705);
            this.Name = "BridesMenu";
            this.Text = "Меню поиска невест";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BridesMenu_FormClosed);
            this.Load += new System.EventHandler(this.BridesMenu_Load);
            this.selectingParametersBox.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBreastSize.ResumeLayout(false);
            this.groupBreastSize.PerformLayout();
            this.positivePropertiesBox.ResumeLayout(false);
            this.positivePropertiesBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox selectingParametersBox;
        private System.Windows.Forms.CheckBox checkCleverPositive;
        private System.Windows.Forms.CheckBox checkRichPositive;
        private System.Windows.Forms.CheckBox checkKindPositive;
        private System.Windows.Forms.CheckBox checkBeautifulPositive;
        private System.Windows.Forms.CheckBox checkPoorPositive;
        private System.Windows.Forms.CheckBox checkUglyPositive;
        private System.Windows.Forms.CheckBox checkAngryPositive;
        private System.Windows.Forms.CheckBox checkStupidPositive;
        private System.Windows.Forms.GroupBox positivePropertiesBox;
        private System.Windows.Forms.ListView listAllBrides;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnProperties;
        private System.Windows.Forms.ListView listSelectedBrides;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label labelAllBrides;
        private System.Windows.Forms.Button buttonSelectBrides;
        private System.Windows.Forms.Label labelSelectedBrides;
        private System.Windows.Forms.Button buttonOpenAddBrideMenu;
        private System.Windows.Forms.Button buttonMainMenu;
        private System.Windows.Forms.GroupBox groupBreastSize;
        private System.Windows.Forms.CheckBox checkAthleticPositive;
        private System.Windows.Forms.CheckBox checkFatPositive;
        private System.Windows.Forms.CheckBox checkSkinnyPositive;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkPoorNegative;
        private System.Windows.Forms.CheckBox checkRichNegative;
        private System.Windows.Forms.CheckBox checkUglyNegative;
        private System.Windows.Forms.CheckBox checkKindNegative;
        private System.Windows.Forms.CheckBox checkCleverNegative;
        private System.Windows.Forms.CheckBox checkBeautifulNegative;
        private System.Windows.Forms.CheckBox checkAngryNegative;
        private System.Windows.Forms.CheckBox checkStupidNegative;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkSkinnyNegative;
        private System.Windows.Forms.CheckBox checkFatNegative;
        private System.Windows.Forms.CheckBox checkAthleticNegative;
        private System.Windows.Forms.CheckBox checkPositiveBreastSize_5;
        private System.Windows.Forms.CheckBox checkPositiveBreastSize_0;
        private System.Windows.Forms.CheckBox checkPositiveBreastSize_4;
        private System.Windows.Forms.CheckBox checkPositiveBreastSize_1;
        private System.Windows.Forms.CheckBox checkPositiveBreastSize_3;
        private System.Windows.Forms.CheckBox checkPositiveBreastSize_2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkNegativeBreastSize_5;
        private System.Windows.Forms.CheckBox checkNegativeBreastSize_0;
        private System.Windows.Forms.CheckBox checkNegativeBreastSize_1;
        private System.Windows.Forms.CheckBox checkNegativeBreastSize_2;
        private System.Windows.Forms.CheckBox checkNegativeBreastSize_4;
        private System.Windows.Forms.CheckBox checkNegativeBreastSize_3;
    }
}