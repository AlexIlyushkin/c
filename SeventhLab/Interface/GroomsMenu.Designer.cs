﻿namespace Ex_1
{
    partial class GroomsMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonMainMenu = new System.Windows.Forms.Button();
            this.buttonAddGroom = new System.Windows.Forms.Button();
            this.labelSelectedGrooms = new System.Windows.Forms.Label();
            this.labelAllGrooms = new System.Windows.Forms.Label();
            this.listSelectedGrooms = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listAllGrooms = new System.Windows.Forms.ListView();
            this.columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnProperties = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.selectingParametersBox = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkSkinnyNegative = new System.Windows.Forms.CheckBox();
            this.checkFatNegative = new System.Windows.Forms.CheckBox();
            this.checkAthleticNegative = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkSkinnyPositive = new System.Windows.Forms.CheckBox();
            this.checkFatPositive = new System.Windows.Forms.CheckBox();
            this.checkAthleticPositive = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkSelfishNegative = new System.Windows.Forms.CheckBox();
            this.checkCaringNegative = new System.Windows.Forms.CheckBox();
            this.checkCowardlyNegative = new System.Windows.Forms.CheckBox();
            this.checkBoldNegative = new System.Windows.Forms.CheckBox();
            this.checkUglyNegative = new System.Windows.Forms.CheckBox();
            this.checkPoorNegative = new System.Windows.Forms.CheckBox();
            this.checkAngryNegative = new System.Windows.Forms.CheckBox();
            this.checkRichNegative = new System.Windows.Forms.CheckBox();
            this.checkCleverNegative = new System.Windows.Forms.CheckBox();
            this.checkKindNegative = new System.Windows.Forms.CheckBox();
            this.checkBeautifulNegative = new System.Windows.Forms.CheckBox();
            this.checkStupidNegative = new System.Windows.Forms.CheckBox();
            this.buttonSelectGrooms = new System.Windows.Forms.Button();
            this.positivePropertiesBox = new System.Windows.Forms.GroupBox();
            this.checkSelfishPositive = new System.Windows.Forms.CheckBox();
            this.checkCaringPositive = new System.Windows.Forms.CheckBox();
            this.checkCowardlyPositive = new System.Windows.Forms.CheckBox();
            this.checkBoldPositive = new System.Windows.Forms.CheckBox();
            this.checkUglyPositive = new System.Windows.Forms.CheckBox();
            this.checkPoorPositive = new System.Windows.Forms.CheckBox();
            this.checkAngryPositive = new System.Windows.Forms.CheckBox();
            this.checkRichPositive = new System.Windows.Forms.CheckBox();
            this.checkCleverPositive = new System.Windows.Forms.CheckBox();
            this.checkKindPositive = new System.Windows.Forms.CheckBox();
            this.checkBeautifulPositive = new System.Windows.Forms.CheckBox();
            this.checkStupidPositive = new System.Windows.Forms.CheckBox();
            this.selectingParametersBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.positivePropertiesBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonMainMenu
            // 
            this.buttonMainMenu.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonMainMenu.Location = new System.Drawing.Point(40, 580);
            this.buttonMainMenu.Name = "buttonMainMenu";
            this.buttonMainMenu.Size = new System.Drawing.Size(161, 35);
            this.buttonMainMenu.TabIndex = 14;
            this.buttonMainMenu.Text = "Главное меню";
            this.buttonMainMenu.UseVisualStyleBackColor = true;
            // 
            // buttonAddGroom
            // 
            this.buttonAddGroom.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddGroom.Location = new System.Drawing.Point(42, 539);
            this.buttonAddGroom.Name = "buttonAddGroom";
            this.buttonAddGroom.Size = new System.Drawing.Size(161, 35);
            this.buttonAddGroom.TabIndex = 13;
            this.buttonAddGroom.Text = "Добавить жениха";
            this.buttonAddGroom.UseVisualStyleBackColor = true;
            // 
            // labelSelectedGrooms
            // 
            this.labelSelectedGrooms.AutoSize = true;
            this.labelSelectedGrooms.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSelectedGrooms.Location = new System.Drawing.Point(568, 9);
            this.labelSelectedGrooms.Name = "labelSelectedGrooms";
            this.labelSelectedGrooms.Size = new System.Drawing.Size(251, 17);
            this.labelSelectedGrooms.TabIndex = 12;
            this.labelSelectedGrooms.Text = "Список предпочтительных женихов";
            // 
            // labelAllGrooms
            // 
            this.labelAllGrooms.AutoSize = true;
            this.labelAllGrooms.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAllGrooms.Location = new System.Drawing.Point(248, 9);
            this.labelAllGrooms.Name = "labelAllGrooms";
            this.labelAllGrooms.Size = new System.Drawing.Size(167, 17);
            this.labelAllGrooms.TabIndex = 11;
            this.labelAllGrooms.Text = "Общий список женихов";
            // 
            // listSelectedGrooms
            // 
            this.listSelectedGrooms.BackColor = System.Drawing.SystemColors.Info;
            this.listSelectedGrooms.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listSelectedGrooms.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listSelectedGrooms.ForeColor = System.Drawing.SystemColors.WindowText;
            this.listSelectedGrooms.GridLines = true;
            this.listSelectedGrooms.HideSelection = false;
            this.listSelectedGrooms.Location = new System.Drawing.Point(562, 29);
            this.listSelectedGrooms.Name = "listSelectedGrooms";
            this.listSelectedGrooms.Size = new System.Drawing.Size(315, 586);
            this.listSelectedGrooms.TabIndex = 10;
            this.listSelectedGrooms.UseCompatibleStateImageBehavior = false;
            this.listSelectedGrooms.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Имя";
            this.columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Черты";
            this.columnHeader2.Width = 600;
            // 
            // listAllGrooms
            // 
            this.listAllGrooms.BackColor = System.Drawing.SystemColors.Info;
            this.listAllGrooms.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnName,
            this.columnProperties});
            this.listAllGrooms.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listAllGrooms.ForeColor = System.Drawing.SystemColors.WindowText;
            this.listAllGrooms.GridLines = true;
            this.listAllGrooms.HideSelection = false;
            this.listAllGrooms.Location = new System.Drawing.Point(241, 29);
            this.listAllGrooms.Name = "listAllGrooms";
            this.listAllGrooms.Size = new System.Drawing.Size(315, 586);
            this.listAllGrooms.TabIndex = 9;
            this.listAllGrooms.UseCompatibleStateImageBehavior = false;
            this.listAllGrooms.View = System.Windows.Forms.View.Details;
            // 
            // columnName
            // 
            this.columnName.Text = "Имя";
            this.columnName.Width = 120;
            // 
            // columnProperties
            // 
            this.columnProperties.Text = "Черты";
            this.columnProperties.Width = 600;
            // 
            // selectingParametersBox
            // 
            this.selectingParametersBox.Controls.Add(this.groupBox3);
            this.selectingParametersBox.Controls.Add(this.groupBox2);
            this.selectingParametersBox.Controls.Add(this.groupBox1);
            this.selectingParametersBox.Controls.Add(this.buttonSelectGrooms);
            this.selectingParametersBox.Controls.Add(this.positivePropertiesBox);
            this.selectingParametersBox.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.selectingParametersBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.selectingParametersBox.Location = new System.Drawing.Point(7, 12);
            this.selectingParametersBox.Name = "selectingParametersBox";
            this.selectingParametersBox.Size = new System.Drawing.Size(228, 511);
            this.selectingParametersBox.TabIndex = 8;
            this.selectingParametersBox.TabStop = false;
            this.selectingParametersBox.Text = "Параметры отбора";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkSkinnyNegative);
            this.groupBox3.Controls.Add(this.checkFatNegative);
            this.groupBox3.Controls.Add(this.checkAthleticNegative);
            this.groupBox3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(28, 390);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(185, 72);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Нежелательная фигура";
            // 
            // checkSkinnyNegative
            // 
            this.checkSkinnyNegative.AutoSize = true;
            this.checkSkinnyNegative.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.checkSkinnyNegative.Location = new System.Drawing.Point(10, 14);
            this.checkSkinnyNegative.Name = "checkSkinnyNegative";
            this.checkSkinnyNegative.Size = new System.Drawing.Size(61, 20);
            this.checkSkinnyNegative.TabIndex = 5;
            this.checkSkinnyNegative.Text = "Дрищ";
            this.checkSkinnyNegative.UseVisualStyleBackColor = true;
            this.checkSkinnyNegative.CheckedChanged += new System.EventHandler(this.checkSkinnyNegative_CheckedChanged);
            // 
            // checkFatNegative
            // 
            this.checkFatNegative.AutoSize = true;
            this.checkFatNegative.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.checkFatNegative.Location = new System.Drawing.Point(10, 32);
            this.checkFatNegative.Name = "checkFatNegative";
            this.checkFatNegative.Size = new System.Drawing.Size(79, 20);
            this.checkFatNegative.TabIndex = 8;
            this.checkFatNegative.Text = "Толстый";
            this.checkFatNegative.UseVisualStyleBackColor = true;
            this.checkFatNegative.CheckedChanged += new System.EventHandler(this.checkFatNegative_CheckedChanged);
            // 
            // checkAthleticNegative
            // 
            this.checkAthleticNegative.AutoSize = true;
            this.checkAthleticNegative.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.checkAthleticNegative.Location = new System.Drawing.Point(10, 49);
            this.checkAthleticNegative.Name = "checkAthleticNegative";
            this.checkAthleticNegative.Size = new System.Drawing.Size(63, 20);
            this.checkAthleticNegative.TabIndex = 4;
            this.checkAthleticNegative.Text = "Качёк";
            this.checkAthleticNegative.UseVisualStyleBackColor = true;
            this.checkAthleticNegative.CheckedChanged += new System.EventHandler(this.checkAthleticNegative_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkSkinnyPositive);
            this.groupBox2.Controls.Add(this.checkFatPositive);
            this.groupBox2.Controls.Add(this.checkAthleticPositive);
            this.groupBox2.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(28, 312);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(185, 72);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Желательная фигура";
            // 
            // checkSkinnyPositive
            // 
            this.checkSkinnyPositive.AutoSize = true;
            this.checkSkinnyPositive.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.checkSkinnyPositive.Location = new System.Drawing.Point(10, 14);
            this.checkSkinnyPositive.Name = "checkSkinnyPositive";
            this.checkSkinnyPositive.Size = new System.Drawing.Size(61, 20);
            this.checkSkinnyPositive.TabIndex = 5;
            this.checkSkinnyPositive.Text = "Дрищ";
            this.checkSkinnyPositive.UseVisualStyleBackColor = true;
            this.checkSkinnyPositive.CheckedChanged += new System.EventHandler(this.checkSkinnyPositive_CheckedChanged);
            // 
            // checkFatPositive
            // 
            this.checkFatPositive.AutoSize = true;
            this.checkFatPositive.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.checkFatPositive.Location = new System.Drawing.Point(10, 32);
            this.checkFatPositive.Name = "checkFatPositive";
            this.checkFatPositive.Size = new System.Drawing.Size(79, 20);
            this.checkFatPositive.TabIndex = 8;
            this.checkFatPositive.Text = "Толстый";
            this.checkFatPositive.UseVisualStyleBackColor = true;
            this.checkFatPositive.CheckedChanged += new System.EventHandler(this.checkFatPositive_CheckedChanged);
            // 
            // checkAthleticPositive
            // 
            this.checkAthleticPositive.AutoSize = true;
            this.checkAthleticPositive.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Italic);
            this.checkAthleticPositive.Location = new System.Drawing.Point(10, 49);
            this.checkAthleticPositive.Name = "checkAthleticPositive";
            this.checkAthleticPositive.Size = new System.Drawing.Size(63, 20);
            this.checkAthleticPositive.TabIndex = 4;
            this.checkAthleticPositive.Text = "Качёк";
            this.checkAthleticPositive.UseVisualStyleBackColor = true;
            this.checkAthleticPositive.CheckedChanged += new System.EventHandler(this.checkAthleticPositive_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkSelfishNegative);
            this.groupBox1.Controls.Add(this.checkCaringNegative);
            this.groupBox1.Controls.Add(this.checkCowardlyNegative);
            this.groupBox1.Controls.Add(this.checkBoldNegative);
            this.groupBox1.Controls.Add(this.checkUglyNegative);
            this.groupBox1.Controls.Add(this.checkPoorNegative);
            this.groupBox1.Controls.Add(this.checkAngryNegative);
            this.groupBox1.Controls.Add(this.checkRichNegative);
            this.groupBox1.Controls.Add(this.checkCleverNegative);
            this.groupBox1.Controls.Add(this.checkKindNegative);
            this.groupBox1.Controls.Add(this.checkBeautifulNegative);
            this.groupBox1.Controls.Add(this.checkStupidNegative);
            this.groupBox1.Location = new System.Drawing.Point(15, 163);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(207, 136);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Нежелательные черты";
            // 
            // checkSelfishNegative
            // 
            this.checkSelfishNegative.AutoSize = true;
            this.checkSelfishNegative.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkSelfishNegative.Location = new System.Drawing.Point(109, 112);
            this.checkSelfishNegative.Name = "checkSelfishNegative";
            this.checkSelfishNegative.Size = new System.Drawing.Size(76, 21);
            this.checkSelfishNegative.TabIndex = 7;
            this.checkSelfishNegative.Text = "Эгоист";
            this.checkSelfishNegative.UseVisualStyleBackColor = true;
            this.checkSelfishNegative.CheckedChanged += new System.EventHandler(this.checkSelfishNegative_CheckedChanged);
            // 
            // checkCaringNegative
            // 
            this.checkCaringNegative.AutoSize = true;
            this.checkCaringNegative.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkCaringNegative.Location = new System.Drawing.Point(6, 112);
            this.checkCaringNegative.Name = "checkCaringNegative";
            this.checkCaringNegative.Size = new System.Drawing.Size(108, 21);
            this.checkCaringNegative.TabIndex = 6;
            this.checkCaringNegative.Text = "Заботливый";
            this.checkCaringNegative.UseVisualStyleBackColor = true;
            this.checkCaringNegative.CheckStateChanged += new System.EventHandler(this.checkCaringNegative_CheckStateChanged);
            // 
            // checkCowardlyNegative
            // 
            this.checkCowardlyNegative.AutoSize = true;
            this.checkCowardlyNegative.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkCowardlyNegative.Location = new System.Drawing.Point(109, 93);
            this.checkCowardlyNegative.Name = "checkCowardlyNegative";
            this.checkCowardlyNegative.Size = new System.Drawing.Size(97, 21);
            this.checkCowardlyNegative.TabIndex = 5;
            this.checkCowardlyNegative.Text = "Трусливый";
            this.checkCowardlyNegative.UseVisualStyleBackColor = true;
            this.checkCowardlyNegative.CheckedChanged += new System.EventHandler(this.checkCowardlyNegative_CheckedChanged);
            // 
            // checkBoldNegative
            // 
            this.checkBoldNegative.AutoSize = true;
            this.checkBoldNegative.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoldNegative.Location = new System.Drawing.Point(6, 93);
            this.checkBoldNegative.Name = "checkBoldNegative";
            this.checkBoldNegative.Size = new System.Drawing.Size(79, 21);
            this.checkBoldNegative.TabIndex = 4;
            this.checkBoldNegative.Text = "Смелый";
            this.checkBoldNegative.UseVisualStyleBackColor = true;
            this.checkBoldNegative.CheckedChanged += new System.EventHandler(this.checkBoldNegative_CheckedChanged);
            // 
            // checkUglyNegative
            // 
            this.checkUglyNegative.AutoSize = true;
            this.checkUglyNegative.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkUglyNegative.Location = new System.Drawing.Point(109, 57);
            this.checkUglyNegative.Name = "checkUglyNegative";
            this.checkUglyNegative.Size = new System.Drawing.Size(87, 21);
            this.checkUglyNegative.TabIndex = 2;
            this.checkUglyNegative.Text = "На разок";
            this.checkUglyNegative.UseVisualStyleBackColor = true;
            this.checkUglyNegative.CheckedChanged += new System.EventHandler(this.checkUglyNegative_CheckedChanged);
            // 
            // checkPoorNegative
            // 
            this.checkPoorNegative.AutoSize = true;
            this.checkPoorNegative.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkPoorNegative.Location = new System.Drawing.Point(109, 75);
            this.checkPoorNegative.Name = "checkPoorNegative";
            this.checkPoorNegative.Size = new System.Drawing.Size(77, 21);
            this.checkPoorNegative.TabIndex = 3;
            this.checkPoorNegative.Text = "Бедный";
            this.checkPoorNegative.UseVisualStyleBackColor = true;
            this.checkPoorNegative.CheckedChanged += new System.EventHandler(this.checkPoorNegative_CheckedChanged);
            // 
            // checkAngryNegative
            // 
            this.checkAngryNegative.AutoSize = true;
            this.checkAngryNegative.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkAngryNegative.Location = new System.Drawing.Point(109, 39);
            this.checkAngryNegative.Name = "checkAngryNegative";
            this.checkAngryNegative.Size = new System.Drawing.Size(57, 21);
            this.checkAngryNegative.TabIndex = 1;
            this.checkAngryNegative.Text = "Злой";
            this.checkAngryNegative.UseVisualStyleBackColor = true;
            this.checkAngryNegative.CheckedChanged += new System.EventHandler(this.checkAngryNegative_CheckedChanged);
            // 
            // checkRichNegative
            // 
            this.checkRichNegative.AutoSize = true;
            this.checkRichNegative.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkRichNegative.Location = new System.Drawing.Point(6, 75);
            this.checkRichNegative.Name = "checkRichNegative";
            this.checkRichNegative.Size = new System.Drawing.Size(86, 21);
            this.checkRichNegative.TabIndex = 2;
            this.checkRichNegative.Text = "Богатый";
            this.checkRichNegative.UseVisualStyleBackColor = true;
            this.checkRichNegative.CheckedChanged += new System.EventHandler(this.checkRichNegative_CheckedChanged);
            // 
            // checkCleverNegative
            // 
            this.checkCleverNegative.AutoSize = true;
            this.checkCleverNegative.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkCleverNegative.Location = new System.Drawing.Point(6, 21);
            this.checkCleverNegative.Name = "checkCleverNegative";
            this.checkCleverNegative.Size = new System.Drawing.Size(72, 21);
            this.checkCleverNegative.TabIndex = 3;
            this.checkCleverNegative.Text = "Умный";
            this.checkCleverNegative.UseVisualStyleBackColor = true;
            this.checkCleverNegative.CheckedChanged += new System.EventHandler(this.checkCleverNegative_CheckedChanged);
            // 
            // checkKindNegative
            // 
            this.checkKindNegative.AutoSize = true;
            this.checkKindNegative.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkKindNegative.Location = new System.Drawing.Point(6, 39);
            this.checkKindNegative.Name = "checkKindNegative";
            this.checkKindNegative.Size = new System.Drawing.Size(79, 21);
            this.checkKindNegative.TabIndex = 1;
            this.checkKindNegative.Text = "Добрый";
            this.checkKindNegative.UseVisualStyleBackColor = true;
            this.checkKindNegative.CheckedChanged += new System.EventHandler(this.checkKindNegative_CheckedChanged);
            // 
            // checkBeautifulNegative
            // 
            this.checkBeautifulNegative.AutoSize = true;
            this.checkBeautifulNegative.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBeautifulNegative.Location = new System.Drawing.Point(6, 57);
            this.checkBeautifulNegative.Name = "checkBeautifulNegative";
            this.checkBeautifulNegative.Size = new System.Drawing.Size(101, 21);
            this.checkBeautifulNegative.TabIndex = 0;
            this.checkBeautifulNegative.Text = "Смазливый";
            this.checkBeautifulNegative.UseVisualStyleBackColor = true;
            this.checkBeautifulNegative.CheckedChanged += new System.EventHandler(this.checkBeautifulNegative_CheckedChanged);
            // 
            // checkStupidNegative
            // 
            this.checkStupidNegative.AutoSize = true;
            this.checkStupidNegative.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkStupidNegative.Location = new System.Drawing.Point(109, 21);
            this.checkStupidNegative.Name = "checkStupidNegative";
            this.checkStupidNegative.Size = new System.Drawing.Size(75, 21);
            this.checkStupidNegative.TabIndex = 0;
            this.checkStupidNegative.Text = "Глупый";
            this.checkStupidNegative.UseVisualStyleBackColor = true;
            this.checkStupidNegative.CheckedChanged += new System.EventHandler(this.checkStupidNegative_CheckedChanged);
            // 
            // buttonSelectGrooms
            // 
            this.buttonSelectGrooms.Location = new System.Drawing.Point(35, 468);
            this.buttonSelectGrooms.Name = "buttonSelectGrooms";
            this.buttonSelectGrooms.Size = new System.Drawing.Size(161, 35);
            this.buttonSelectGrooms.TabIndex = 4;
            this.buttonSelectGrooms.Text = "Найти кандидатов";
            this.buttonSelectGrooms.UseVisualStyleBackColor = true;
            this.buttonSelectGrooms.Click += new System.EventHandler(this.buttonSelectGrooms_Click);
            // 
            // positivePropertiesBox
            // 
            this.positivePropertiesBox.Controls.Add(this.checkSelfishPositive);
            this.positivePropertiesBox.Controls.Add(this.checkCaringPositive);
            this.positivePropertiesBox.Controls.Add(this.checkCowardlyPositive);
            this.positivePropertiesBox.Controls.Add(this.checkBoldPositive);
            this.positivePropertiesBox.Controls.Add(this.checkUglyPositive);
            this.positivePropertiesBox.Controls.Add(this.checkPoorPositive);
            this.positivePropertiesBox.Controls.Add(this.checkAngryPositive);
            this.positivePropertiesBox.Controls.Add(this.checkRichPositive);
            this.positivePropertiesBox.Controls.Add(this.checkCleverPositive);
            this.positivePropertiesBox.Controls.Add(this.checkKindPositive);
            this.positivePropertiesBox.Controls.Add(this.checkBeautifulPositive);
            this.positivePropertiesBox.Controls.Add(this.checkStupidPositive);
            this.positivePropertiesBox.Location = new System.Drawing.Point(15, 22);
            this.positivePropertiesBox.Name = "positivePropertiesBox";
            this.positivePropertiesBox.Size = new System.Drawing.Size(207, 135);
            this.positivePropertiesBox.TabIndex = 4;
            this.positivePropertiesBox.TabStop = false;
            this.positivePropertiesBox.Text = "Желательные черты";
            // 
            // checkSelfishPositive
            // 
            this.checkSelfishPositive.AutoSize = true;
            this.checkSelfishPositive.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkSelfishPositive.Location = new System.Drawing.Point(109, 112);
            this.checkSelfishPositive.Name = "checkSelfishPositive";
            this.checkSelfishPositive.Size = new System.Drawing.Size(76, 21);
            this.checkSelfishPositive.TabIndex = 7;
            this.checkSelfishPositive.Text = "Эгоист";
            this.checkSelfishPositive.UseVisualStyleBackColor = true;
            this.checkSelfishPositive.CheckedChanged += new System.EventHandler(this.checkSelfishPositive_CheckedChanged);
            // 
            // checkCaringPositive
            // 
            this.checkCaringPositive.AutoSize = true;
            this.checkCaringPositive.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkCaringPositive.Location = new System.Drawing.Point(6, 112);
            this.checkCaringPositive.Name = "checkCaringPositive";
            this.checkCaringPositive.Size = new System.Drawing.Size(108, 21);
            this.checkCaringPositive.TabIndex = 6;
            this.checkCaringPositive.Text = "Заботливый";
            this.checkCaringPositive.UseVisualStyleBackColor = true;
            this.checkCaringPositive.CheckedChanged += new System.EventHandler(this.checkCaringPositive_CheckedChanged);
            // 
            // checkCowardlyPositive
            // 
            this.checkCowardlyPositive.AutoSize = true;
            this.checkCowardlyPositive.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkCowardlyPositive.Location = new System.Drawing.Point(109, 93);
            this.checkCowardlyPositive.Name = "checkCowardlyPositive";
            this.checkCowardlyPositive.Size = new System.Drawing.Size(97, 21);
            this.checkCowardlyPositive.TabIndex = 5;
            this.checkCowardlyPositive.Text = "Трусливый";
            this.checkCowardlyPositive.UseVisualStyleBackColor = true;
            this.checkCowardlyPositive.CheckedChanged += new System.EventHandler(this.checkCowardlyPositive_CheckedChanged);
            // 
            // checkBoldPositive
            // 
            this.checkBoldPositive.AutoSize = true;
            this.checkBoldPositive.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoldPositive.Location = new System.Drawing.Point(6, 93);
            this.checkBoldPositive.Name = "checkBoldPositive";
            this.checkBoldPositive.Size = new System.Drawing.Size(79, 21);
            this.checkBoldPositive.TabIndex = 4;
            this.checkBoldPositive.Text = "Смелый";
            this.checkBoldPositive.UseVisualStyleBackColor = true;
            this.checkBoldPositive.CheckedChanged += new System.EventHandler(this.checkBoldPositive_CheckedChanged);
            // 
            // checkUglyPositive
            // 
            this.checkUglyPositive.AutoSize = true;
            this.checkUglyPositive.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkUglyPositive.Location = new System.Drawing.Point(109, 57);
            this.checkUglyPositive.Name = "checkUglyPositive";
            this.checkUglyPositive.Size = new System.Drawing.Size(87, 21);
            this.checkUglyPositive.TabIndex = 2;
            this.checkUglyPositive.Text = "На разок";
            this.checkUglyPositive.UseVisualStyleBackColor = true;
            this.checkUglyPositive.CheckedChanged += new System.EventHandler(this.checkUglyPositive_CheckedChanged);
            // 
            // checkPoorPositive
            // 
            this.checkPoorPositive.AutoSize = true;
            this.checkPoorPositive.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkPoorPositive.Location = new System.Drawing.Point(109, 75);
            this.checkPoorPositive.Name = "checkPoorPositive";
            this.checkPoorPositive.Size = new System.Drawing.Size(77, 21);
            this.checkPoorPositive.TabIndex = 3;
            this.checkPoorPositive.Text = "Бедный";
            this.checkPoorPositive.UseVisualStyleBackColor = true;
            this.checkPoorPositive.CheckedChanged += new System.EventHandler(this.checkPoorPositive_CheckedChanged);
            // 
            // checkAngryPositive
            // 
            this.checkAngryPositive.AutoSize = true;
            this.checkAngryPositive.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkAngryPositive.Location = new System.Drawing.Point(109, 39);
            this.checkAngryPositive.Name = "checkAngryPositive";
            this.checkAngryPositive.Size = new System.Drawing.Size(57, 21);
            this.checkAngryPositive.TabIndex = 1;
            this.checkAngryPositive.Text = "Злой";
            this.checkAngryPositive.UseVisualStyleBackColor = true;
            this.checkAngryPositive.CheckedChanged += new System.EventHandler(this.checkAngryPositive_CheckedChanged);
            // 
            // checkRichPositive
            // 
            this.checkRichPositive.AutoSize = true;
            this.checkRichPositive.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkRichPositive.Location = new System.Drawing.Point(6, 75);
            this.checkRichPositive.Name = "checkRichPositive";
            this.checkRichPositive.Size = new System.Drawing.Size(86, 21);
            this.checkRichPositive.TabIndex = 2;
            this.checkRichPositive.Text = "Богатый";
            this.checkRichPositive.UseVisualStyleBackColor = true;
            this.checkRichPositive.CheckedChanged += new System.EventHandler(this.checkRichPositive_CheckedChanged);
            // 
            // checkCleverPositive
            // 
            this.checkCleverPositive.AutoSize = true;
            this.checkCleverPositive.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkCleverPositive.Location = new System.Drawing.Point(6, 21);
            this.checkCleverPositive.Name = "checkCleverPositive";
            this.checkCleverPositive.Size = new System.Drawing.Size(72, 21);
            this.checkCleverPositive.TabIndex = 3;
            this.checkCleverPositive.Text = "Умный";
            this.checkCleverPositive.UseVisualStyleBackColor = true;
            this.checkCleverPositive.CheckedChanged += new System.EventHandler(this.checkCleverPositive_CheckedChanged);
            // 
            // checkKindPositive
            // 
            this.checkKindPositive.AutoSize = true;
            this.checkKindPositive.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkKindPositive.Location = new System.Drawing.Point(6, 39);
            this.checkKindPositive.Name = "checkKindPositive";
            this.checkKindPositive.Size = new System.Drawing.Size(79, 21);
            this.checkKindPositive.TabIndex = 1;
            this.checkKindPositive.Text = "Добрый";
            this.checkKindPositive.UseVisualStyleBackColor = true;
            this.checkKindPositive.CheckedChanged += new System.EventHandler(this.checkKindPositive_CheckedChanged);
            // 
            // checkBeautifulPositive
            // 
            this.checkBeautifulPositive.AutoSize = true;
            this.checkBeautifulPositive.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBeautifulPositive.Location = new System.Drawing.Point(6, 57);
            this.checkBeautifulPositive.Name = "checkBeautifulPositive";
            this.checkBeautifulPositive.Size = new System.Drawing.Size(101, 21);
            this.checkBeautifulPositive.TabIndex = 0;
            this.checkBeautifulPositive.Text = "Смазливый";
            this.checkBeautifulPositive.UseVisualStyleBackColor = true;
            this.checkBeautifulPositive.CheckedChanged += new System.EventHandler(this.checkBeautifulPositive_CheckedChanged);
            // 
            // checkStupidPositive
            // 
            this.checkStupidPositive.AutoSize = true;
            this.checkStupidPositive.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkStupidPositive.Location = new System.Drawing.Point(109, 21);
            this.checkStupidPositive.Name = "checkStupidPositive";
            this.checkStupidPositive.Size = new System.Drawing.Size(75, 21);
            this.checkStupidPositive.TabIndex = 0;
            this.checkStupidPositive.Text = "Глупый";
            this.checkStupidPositive.UseVisualStyleBackColor = true;
            this.checkStupidPositive.CheckedChanged += new System.EventHandler(this.checkStupidPositive_CheckedChanged);
            // 
            // GroomsMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(884, 627);
            this.Controls.Add(this.buttonMainMenu);
            this.Controls.Add(this.buttonAddGroom);
            this.Controls.Add(this.labelSelectedGrooms);
            this.Controls.Add(this.labelAllGrooms);
            this.Controls.Add(this.listSelectedGrooms);
            this.Controls.Add(this.listAllGrooms);
            this.Controls.Add(this.selectingParametersBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximumSize = new System.Drawing.Size(900, 1000);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(900, 507);
            this.Name = "GroomsMenu";
            this.Text = "Меню поиска женихов";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GroomsMenu_FormClosed);
            this.Load += new System.EventHandler(this.GroomsMenu_Load);
            this.selectingParametersBox.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.positivePropertiesBox.ResumeLayout(false);
            this.positivePropertiesBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonMainMenu;
        private System.Windows.Forms.Button buttonAddGroom;
        private System.Windows.Forms.Label labelSelectedGrooms;
        private System.Windows.Forms.Label labelAllGrooms;
        private System.Windows.Forms.ListView listSelectedGrooms;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ListView listAllGrooms;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnProperties;
        private System.Windows.Forms.GroupBox selectingParametersBox;
        private System.Windows.Forms.Button buttonSelectGrooms;
        private System.Windows.Forms.CheckBox checkPoorPositive;
        private System.Windows.Forms.CheckBox checkUglyPositive;
        private System.Windows.Forms.CheckBox checkAngryPositive;
        private System.Windows.Forms.CheckBox checkStupidPositive;
        private System.Windows.Forms.GroupBox positivePropertiesBox;
        private System.Windows.Forms.CheckBox checkRichPositive;
        private System.Windows.Forms.CheckBox checkCleverPositive;
        private System.Windows.Forms.CheckBox checkKindPositive;
        private System.Windows.Forms.CheckBox checkBeautifulPositive;
        private System.Windows.Forms.CheckBox checkCowardlyPositive;
        private System.Windows.Forms.CheckBox checkBoldPositive;
        private System.Windows.Forms.CheckBox checkSelfishPositive;
        private System.Windows.Forms.CheckBox checkCaringPositive;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkSelfishNegative;
        private System.Windows.Forms.CheckBox checkCaringNegative;
        private System.Windows.Forms.CheckBox checkCowardlyNegative;
        private System.Windows.Forms.CheckBox checkBoldNegative;
        private System.Windows.Forms.CheckBox checkUglyNegative;
        private System.Windows.Forms.CheckBox checkPoorNegative;
        private System.Windows.Forms.CheckBox checkAngryNegative;
        private System.Windows.Forms.CheckBox checkRichNegative;
        private System.Windows.Forms.CheckBox checkCleverNegative;
        private System.Windows.Forms.CheckBox checkKindNegative;
        private System.Windows.Forms.CheckBox checkBeautifulNegative;
        private System.Windows.Forms.CheckBox checkStupidNegative;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkSkinnyNegative;
        private System.Windows.Forms.CheckBox checkFatNegative;
        private System.Windows.Forms.CheckBox checkAthleticNegative;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkSkinnyPositive;
        private System.Windows.Forms.CheckBox checkFatPositive;
        private System.Windows.Forms.CheckBox checkAthleticPositive;
    }
}