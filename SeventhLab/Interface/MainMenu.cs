﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex_1
{
    public partial class MainMenu : Form
    {
        #region Constructors
        public MainMenu()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        private void openBrideMenuButton_Click(object sender, EventArgs e)
        {
            Hide();
            BridesMenu bridesMenu = new BridesMenu();
            bridesMenu.Owner = this;
            bridesMenu.Show();
        }

        private void openGroomMenuButton_Click(object sender, EventArgs e)
        {
            Hide();
            GroomsMenu groomsMenu = new GroomsMenu();
            groomsMenu.Owner = this;
            groomsMenu.Show();
        }
        #endregion
    }
}
