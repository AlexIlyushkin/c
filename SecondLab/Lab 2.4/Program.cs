﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_2._4
{
    class Program
    {
        static void Main(string[] args)
        {
            const decimal S = 55000000M; //минимальное расстояние от Земли до Марса в км
            Console.WriteLine("Минимальное расстояние от Земли до Марса: " + S);
            const decimal A = 1406.07M; //аршинов в одном км
            Console.WriteLine("Аршинов в одном км: " + A);
            decimal SA = S * A;
            Console.WriteLine("Расстояние от Земли до Марса в аршинах: " + SA);
        }  

        
    }
}
