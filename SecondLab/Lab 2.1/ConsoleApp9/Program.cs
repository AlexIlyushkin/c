﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите значение X: ");
            int x = int.Parse(Console.ReadLine());
            int y = x;
            x *= x;
            int z = x;
            x *= x;
            x *= z;
            x *= y;
            Console.WriteLine("x^7 = " + x);
        }
    }
}
