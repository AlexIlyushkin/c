﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите натуральное число:");
            uint value = uint.Parse(Console.ReadLine());

            value = value / 10 % 10;

            Console.WriteLine("Число десятков в его десятичной записи: " + value);
        }
    }
}
