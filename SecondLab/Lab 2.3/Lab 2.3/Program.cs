﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_2._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите значение переменной a: ");
            double a = double.Parse(Console.ReadLine());

            Console.WriteLine("Введите значение переменной b: ");
            double b = double.Parse(Console.ReadLine());

            Console.WriteLine("Введите значение переменной c: ");
            double c = double.Parse(Console.ReadLine());

            Console.WriteLine("Введите значенеи переменной d: ");
            double d = double.Parse(Console.ReadLine());

            Console.WriteLine("Введите значение переменной e: ");
            double e = double.Parse(Console.ReadLine());

            Console.WriteLine("Введите значение переменной n: ");
            double n = double.Parse(Console.ReadLine());

            Console.WriteLine("Введите значение переменной m: ");
            double m = double.Parse(Console.ReadLine());

            Console.WriteLine("Введите значение переменной p: ");
            double p = double.Parse(Console.ReadLine());

            double x = Function(a, b, c, d, e, n, m, p);
            Console.WriteLine("Значение x = {0}", x);
        }


        static double Function(double a, double b, double c, double d, double e, double n, double m, double p)
        {
            double t1 = Math.Sin(a);
            double t2 = b * c / Math.Pow(d, m);
            double t3 = Math.Cos(t2);
            const double t4 = 1 / 3;

            double res = (Math.Pow(2, n) * ((t1 + t3) * (t1 - t3))) / (Math.Log10(Math.Pow(a + t2, t4)) - e / Math.Pow(2, p));
            return res;
        }





        }
    }
