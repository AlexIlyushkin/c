﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите целое значение x: ");
            int x = int.Parse(Console.ReadLine());

            Console.WriteLine("Введите значение a: ");
            double a = double.Parse(Console.ReadLine());

            Console.WriteLine("Введите значение b: ");
            double b = double.Parse(Console.ReadLine());

            Console.WriteLine("Введите значение c: ");
            double c = double.Parse(Console.ReadLine());

            double F = Function(x, a, b, c);
            Console.WriteLine("Значение функции F = " + F);

        }

        static double Function(int x, double a, double b, double c)
        {
            if (a < 0 && x != 0)
                return -3 * a / x - c;
            else if (c < 0 && a == 0)
                return (x - b) / (-c);
            else
                return c * (b - a) + x;
        }

    }
}
