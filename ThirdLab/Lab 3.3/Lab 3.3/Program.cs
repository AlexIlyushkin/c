﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите натуральное значение: ");
            double value = double.Parse(Console.ReadLine());

            Console.WriteLine("Введите длину прогрессии: ");
            uint lengthProgression = uint.Parse(Console.ReadLine());

            Console.WriteLine("Сумма прогрессии: " + SumOfProgression(value, lengthProgression));
        }

        static double SumOfProgression(double value, uint lengthProgression)
        {
            double sum = 0;

            for(int i = 0; i <= lengthProgression; i++)
            {
                sum += Math.Pow(value, i);
            }

            return sum;
        }
    }
}
