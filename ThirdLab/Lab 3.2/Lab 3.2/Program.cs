﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите натуральное число: ");
            int value = int.Parse(Console.ReadLine());

            Console.WriteLine("Сумма первых {0} членов ряда: {1}", value, SumOfRow(value));
        }

        static double SumOfRow(int lengthRow)
        {
            double sum = 0;

            for (int k = 1; k <= lengthRow; k++) 
            {
                if (k % 4 != 0)
                    sum += 1.0 / (k * (k + 1));
            }

            return sum;
        }

    }
}
