﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(PrimeIs(uint.Parse(Console.ReadLine())));
        }

        static bool PrimeIs(uint value, uint divider = 2)
        {
            if(value % divider == 0 && divider != value || value == 0 || value == 1)
            {
                return false;
            }
            else if(divider == value)
            {
                return true;
            }
            else
            {
                PrimeIs(value, divider + 1);
            }

            return false;
        }
    }
}
