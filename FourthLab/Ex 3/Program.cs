﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(UpToTen(int.Parse(Console.ReadLine())));
        }

        static int UpToTen(int value)
        {
            int weight = 1, digitOfValue, result = 0;

            while (value != 0)
            {
                digitOfValue = value % 10;

                if (digitOfValue != 0)
                {
                    digitOfValue = 10 - digitOfValue;
                    result += digitOfValue * weight;
                    value /= 10;
                }
                else
                {
                    value /= 10;
                }

                weight *= 10;
            }
            return result;
        }
    }
}
