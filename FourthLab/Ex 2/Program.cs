﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = SumInputValues();
            Console.WriteLine($"Сумма введённых чисел равна: {sum}");
        }

        static int SumInputValues()
        {
            int value, sum = 0;

            value = int.Parse(Console.ReadLine());

            while (value != 0)
            {
                sum += value;       
                value = int.Parse(Console.ReadLine());
            }

            return sum;
        }
    }
}
