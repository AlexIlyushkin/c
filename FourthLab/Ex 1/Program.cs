﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите натуральное число: ");
            int value = int.Parse(Console.ReadLine());
            Console.WriteLine("Сумма цифр числа " + value + " = " + GetSumOfDigits(value));
        }

        static int GetSumOfDigits(int value)
        {
            int sum = 0;
            int Digit = 0;

            while (value != 0)
            {
                Digit = value % 10;
                sum += Digit;
                value /= 10;
            }

            return sum;
        }
    }
}
