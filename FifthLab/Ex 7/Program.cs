﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите длину строки: ");
            StringBuilder stringBuilder = Fill(int.Parse(Console.ReadLine()));

            Console.WriteLine("Строка: ");
            Console.WriteLine(stringBuilder.ToString());

            stringBuilder.Replace(';', '_');
            Console.WriteLine("Строка с заменой ; на _ :");
            Console.WriteLine(stringBuilder.ToString());
        }

        static StringBuilder Fill(int length)
        {
            Random random = new Random();
            string line = ""; 
            char[] alphabet = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '1', '2', '3', '4', '5', '6', '7', '?', '!', ';' };

            for (int i = 0; i < length; i++)
            {
                line += alphabet[random.Next(0, alphabet.Length - 1)];
            }

            StringBuilder stringBuilder = new StringBuilder(line);

            return stringBuilder;
        }       
    }
}
