﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_3__new_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите кол-во элементов массива А: ");
            int[] array_A = new int[int.Parse(Console.ReadLine())];
            Fill(array_A);
            Sort(array_A);
            Print(array_A);

            Console.WriteLine("Введите кол-во элементов массива В: ");
            int[] array_B = new int[int.Parse(Console.ReadLine())];
            Fill(array_B);
            Sort(array_B);
            Print(array_B);

            int[] array_C = MergeArrays(array_A, array_B);
            Print(array_C);
        }

        static void Fill(int[] array)
        {
            Random rnd = new Random();

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(0, 20);
            }            
        }

        static void Sort(int[] array)
        {
            int temp;

            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length - 1 - i; j++)
                {
                    if (array[j] > array[j + 1])
                    {
                        temp = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = temp;
                    }
                }
            }
        }

        static void Print(int[] array)
        {
            foreach (int Element in array)
            {
                Console.Write(Element + "  ");
            }
            Console.WriteLine();
        }

        static int[] MergeArrays(int[] arrayFirst, int[] arraySecond)
        {
            int[] newArray = new int[arrayFirst.Length + arraySecond.Length];

            int position = 0, i = 0, j = 0;

            while (i < arrayFirst.Length && j < arraySecond.Length)
            {
                if(arrayFirst[i] < arraySecond[j])
                {
                    newArray[position++] = arrayFirst[i++];
                }
                else
                {
                    newArray[position++] = arraySecond[j++];
                }
            }

            while (i < arrayFirst.Length)
            {
                newArray[position++] = arrayFirst[i++];
            }

            while (j < arraySecond.Length)
            {
                newArray[position++] = arraySecond[j++];
            }

            return newArray;
        }
    }
}
