﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_8
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][,] stepArray = new int[3][,];

            stepArray[0] = new int[2, 2];
            Fill(stepArray[0]);
            Print(stepArray[0]);
            Console.WriteLine();

            stepArray[1] = new int[3, 2];
            Fill(stepArray[1]);
            Print(stepArray[1]);
            Console.WriteLine();

            stepArray[2] = new int[2, 3];
            Fill(stepArray[2]);
            Print(stepArray[2]);
            Console.WriteLine();

            Console.WriteLine("Сумма всех минимальных элементов в столбцах: " + SumOfMinElements(stepArray));

        }

        static void Fill(int[,] matrix)
        {
            Random rand = new Random();

            for(int i = 0; i < matrix.GetLength(0); i++)
            {
                for(int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = rand.Next(0, 20);
                }
            }
        }

        static void Print(int[,] matrix)
        {
            for(int i = 0; i < matrix.GetLength(0); i++)
            {
                for(int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + "   ");
                }
                Console.WriteLine();
            }
        }

        static int SumOfMinElements(int[][,] stepArray)
        {
            int sum = 0;
            int min = 0;

            for(int indexMatrix = 0; indexMatrix < stepArray.GetLength(0); indexMatrix++)
            {
                for (int j = 0; j < stepArray[indexMatrix].GetLength(1); j++)
                {
                    min = stepArray[indexMatrix][0, j];

                    for (int i = 1; i < stepArray[indexMatrix].GetLength(0); i++)
                    {
                        if(stepArray[indexMatrix][i, j] < min)
                        {
                            min = stepArray[indexMatrix][i, j];
                        }
                    }

                    sum += min;
                }
            }

            return sum;
        }       
    }
}
