﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите кол-во строк матрицы: ");
            uint countRows = uint.Parse(Console.ReadLine());

            Console.WriteLine("Введите кол-во столбцов матрицы: ");
            uint countColumns = uint.Parse(Console.ReadLine());

            double[,] matrix = new double[countRows, countColumns];
            Fill(matrix);
            Print(matrix);
            Console.WriteLine();

            SmoothMatrix(ref matrix);
            Print(matrix);

        }

        static void Fill(double[,] matrix)
        {
            Random rand = new Random();

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = rand.Next(0, 20);
                }
            }
        }

        static void Print(double[,] matrix)
        {
            Console.WriteLine();

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + "   ");
                }
                Console.WriteLine();
            }
        }

        static void SmoothMatrix(ref double[,] matrix)
        {
            double[,] newMatrix = new double[matrix.GetLength(0), matrix.GetLength(1)];
            int maxColumn = matrix.GetLength(0) - 1;
            int maxRow = matrix.GetLength(1) - 1;

            newMatrix[0, 0] = (matrix[0, 1] + matrix[1, 1] + matrix[1, 0]) / 3;
            newMatrix[0, maxRow] = (matrix[0, maxRow - 1] + matrix[1, maxRow - 1] + matrix[1, maxRow]) / 3;
            newMatrix[maxColumn, 0] = (matrix[maxColumn - 1, 0] + matrix[maxColumn - 1, 1] + matrix[maxColumn, 1]) / 3;
            newMatrix[maxColumn, maxRow] = (matrix[maxColumn - 1, maxRow] + matrix[maxColumn - 1, maxRow - 1] + matrix[maxColumn, maxRow - 1]) / 3;

            for(int j = 1; j < maxRow; j++)
            {
                newMatrix[0, j] = (matrix[0, j - 1] + matrix[1, j - 1] + matrix[1, j] + matrix[1, j + 1] + matrix[0, j + 1]) / 5;
                newMatrix[maxColumn, j] = (matrix[maxColumn, j - 1] + matrix[maxColumn - 1, j - 1] + matrix[maxColumn - 1, j] + matrix[maxColumn - 1, j + 1] + matrix[maxColumn, j + 1]) / 5;
            }

            for(int i = 1; i < maxColumn; i++)
            {
                newMatrix[i, maxRow] = (matrix[i - 1, maxRow] + matrix[i - 1, maxRow - 1] + matrix[i, maxRow - 1] + matrix[i + 1, maxRow] + matrix[i + 1, maxRow]) / 5;
                newMatrix[i, 0] = (matrix[i - 1, 0] + matrix[i - 1, 1] + matrix[i, 1] + matrix[i + 1, 1] + matrix[i + 1, 0]) / 5;
            }

            for(int i = 1; i < maxColumn - 1; i++)
            {
                for (int j = 1; j < maxRow - 1; j++)
                {
                    newMatrix[i, j] = (matrix[i - 1, j] + matrix[i - 1, j + 1] + matrix[i, j + 1] + matrix[i + 1, j + 1] + matrix[i + 1, j] + matrix[i + 1, j - 1] + matrix[i, j - 1] + matrix[i - 1, j - 1]) / 8;
                }
            }

            matrix = newMatrix;
        }
    }
}
