﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите первую строку: ");
            string firstString = Console.ReadLine();

            Console.WriteLine("Введите вторую строку: ");
            string secondString = Console.ReadLine();

            if (ContainsSubstring(firstString, secondString))
            {
                Console.WriteLine("Первая строка является подстрокой второй строки");
            }
            else
            {
                Console.WriteLine("Первая подстрока не явялется подстрокой второй строки");
            }

        }        

        static bool ContainsSubstring(string firstString, string secondString)
        {
            int[] pi = ArrayPiOf(firstString);                    

            int indexSecond = 0;
            int indexFirst = 0;
            
            while(indexSecond != secondString.Length)
            {
                if(secondString[indexSecond] == firstString[indexFirst])
                {
                    indexSecond++;
                    indexFirst++;

                    if (indexFirst == firstString.Length)
                    {
                        return true;
                    }                                            
                }
                else if(indexFirst == 0)
                {
                    indexSecond++;                    
                }
                else
                {
                    indexFirst = pi[indexFirst - 1];
                }

                if (indexSecond == secondString.Length)
                {
                    return false;
                }
            }

            return false;
        }

        static int[] ArrayPiOf(string line)
        {           
            int[] pi = new int[line.Length];
            pi[0] = 0;

            int j = 0;
            int i = 1;

            while(i < line.Length)
            {
                if (line[i] == line[j])
                {
                    pi[i] = j + 1;
                    j++;
                    i++;
                }
                else if (j == 0)
                {
                    pi[i] = 0;
                    i++;
                }
                else
                {
                    j = pi[j - 1];
                }
            }

            return pi;
        }
    }
}
