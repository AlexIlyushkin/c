﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_2__new_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите кол-во элементов масссива: ");
            int?[] array = new int?[int.Parse(Console.ReadLine())];
            Fill(array);
            Print(array);

            Console.WriteLine("Ввведите индекс элемента, который хотите удаплить: ");
            DeleteElement(array, int.Parse(Console.ReadLine()));
            Print(array);
            
        }

        static void Fill(int?[] array)
        {
            for(int i = 0; i < array.Length; i++)
            {
                array[i] = i;
            }
        }

        static void Print(int?[] array)
        {
            foreach(int? Element in array)
            {
                Console.Write(Element + "  ");
            }
            Console.WriteLine();
        }

        static void DeleteElement(int?[] array, int index)
        {
            for(int i = index; i < array.Length - 1; i++)
            {
                array[i] = array[i + 1];
            }

            array[array.Length - 1] = null;
        }
    }
}
