﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите кол-во строк матрицы: ");
            uint countRows = uint.Parse(Console.ReadLine());

            Console.WriteLine("Введите кол-во столбцов матрицы: ");
            uint countColumns = uint.Parse(Console.ReadLine());

            int[,] matrix = new int[countRows, countColumns];
            Fill(matrix);

            Console.WriteLine();

            Console.WriteLine("Матрица: ");

            Print(matrix);

            PrintMaxElements(matrix);

            Console.WriteLine();
            Console.WriteLine("Среднее значение нечётных элементов матрицы: " + GetAverageOfOddElements(matrix));
        }

        static void Fill(int[,] matrix)
        {
            Random rand = new Random();

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = rand.Next(0, 20);
                }
            }
        }

        static void Print(int[,] matrix)
        {
            Console.WriteLine();
            for(int i = 0; i < matrix.GetLength(0); i++)
            {
                for(int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + "   ");
                }
                Console.WriteLine();
            }
        }

        static void PrintMaxElements(int[,] matrix)
        {
            Console.WriteLine();

            int max;

            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                max = matrix[0, j];

                for (int i = 1; i < matrix.GetLength(0); i++)
                {
                    if (matrix[i, j] > max)
                        max = matrix[i, j];
                }
                Console.WriteLine("Максимальный элемент " + (++j) + " столбца матрицы: " + max);
            }
        }

        static double GetAverageOfOddElements(int[,] matrix)
        {
            int sum = 0;
            uint quantity = 0;

            foreach(int item in matrix)
            {
                if(item % 2 != 0)
                {
                    sum += item;
                    ++quantity;
                }
            }

            return sum / quantity;
        }
    }
}
