﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] values = new int[15];
            Fill(values);
            Print(values);
        }

        static void Fill(int[] array)
        {
            Random random = new Random();

            for (byte i = 0; i < 15; i++)
            {
                array[i] = random.Next(-50, 50);
            }
        }

        static void Print(int[] array)
        {
            foreach(int item in array)
            {
                Console.Write(item + "  "); 
            }
        }

        static byte CountValuesMultiplesOfThree(int[] array)
        {
            byte counter = 0;
            foreach (int item in array)
            {
                if (item % 3 == 0)
                    ++counter;
            }
            return counter;
        }

        static int SumOfPositiveValues(int[] array)
        {
            int sum = 0;

            foreach (int item in array)
            {
                if (item > 0) sum += item; 
            }
            return sum;
        }

        static int GetIndexOfMax(int[] array)
        {
            int indexOfMax = 0;
            int max = array[0];

            for (byte i = 1; i < 15; i++)
            {
                if (array[i] > max)
                {
                    max = array[i];
                    indexOfMax = i;
                }
            }
            return indexOfMax;
        }

        static bool ContainsZero(int[] array)
        {
            foreach (int item in array)
            {
                if (item == 0)
                {
                    return true;
                }
            }
            return false;
        }

        static void EvenIndexesOfArray(int[] array)
        {
            for (byte i = 0; i < 15; i++)
            {
                if (i % 2 == 0)
                    Console.WriteLine($"a[{i}]= {array[i]}");
            }
        }
    }
}
