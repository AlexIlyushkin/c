﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_1
{
    class Time
    {
        #region Constants
        private const int DEAFAULT_HOURS = 0;
        private const int DEAFAULT_MINUTES = 0;
        private const int DEAFAULT_SECONDS = 0;
        #endregion

        #region Attributes
        private int hours;
        private int minutes;
        private int seconds;
        #endregion

        #region Constructors
        public Time() : this(DEAFAULT_HOURS, DEAFAULT_MINUTES, DEAFAULT_SECONDS) { }

        public Time(int hours) : this(hours, DEAFAULT_MINUTES, DEAFAULT_SECONDS) { }

        public Time(int hours, int minutes) : this(hours, minutes, DEAFAULT_SECONDS) { }

        public Time(int hours, int minutes, int seconds)
        {
            this.Hours = hours;
            this.Minutes = minutes;
            this.Seconds = seconds;
        }
        #endregion

        #region Properties
        public int Hours
        {
            set
            {
                if (value < 24 && value >= 0)
                {
                    hours = value;
                }
                else
                {
                    throw new ArgumentException("Hours can't be less than 0 and more than 23");
                }
            }
            get
            {
                return hours;
            }
        }


        public int Minutes
        {
            set
            {
                if (value < 60 && value >= 0)
                {
                    minutes = value;
                }
                else
                {
                    throw new ArgumentException("Minutes can't be less than 0 and more than 59");
                }
            }
            get
            {
                return minutes;
            }
        }


        public int Seconds
        {
            set
            {
                if (value < 60 && value >= 0)
                {
                    seconds = value;
                }
                else
                {
                    throw new ArgumentException("Seconds can't be less than 0 and more than 59");
                }
            }
            get
            {
                return seconds;
            }
        }
        #endregion

        #region Methods
        public void ChangeHours(int hours)
        {
            Hours = this.hours + hours;
        }

        public void ChangeMinutes(int minutes)
        {
            if (this.minutes + minutes < 0)
            {
                Hours -= (this.minutes - minutes) / 60;
                Minutes = 60 + (this.minutes + minutes) % 60;
            }
            else if (this.minutes + minutes >= 60)
            {
                Hours += (this.minutes + minutes) / 60;
                Minutes = (this.minutes + minutes) % 60;
            }
            else
            {
                Minutes += minutes;
            }
        }

        public void ChangeSeconds(int seconds)
        {
            if (this.seconds + seconds < 0)
            {
                Minutes -= (this.seconds - seconds) / 60;
                Seconds = 60 + (this.seconds + seconds) % 60;
            }
            else if (this.seconds + seconds >= 60)
            {
                Minutes += (this.seconds + seconds) / 60;
                Seconds = (this.seconds + seconds) % 60;
            }
            else
            {
                Seconds += seconds;
            }
        }

        public void Print()
        {
            Console.WriteLine(this.ToString());
        }

        public static void Print(Time time)
        {
            Console.WriteLine("{0}{1}:{2}{3}:{4}{5}", time.Hours / 10, time.Hours % 10, time.Minutes / 10, time.Minutes % 10, time.Seconds / 10, time.Seconds % 10);
        }

        public override string ToString()
        {
            return $"{hours / 10}{hours % 10}:{minutes / 10}{minutes % 10}:{seconds / 10}{seconds % 10}";
        }
        #endregion
    }
}
