﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;


namespace Ex_1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Time time = new Time();
                Time.Print(time);

                Console.WriteLine("Введите кол-во часов: ");
                time.Hours = int.Parse(Console.ReadLine());

                Console.WriteLine("Введи кол-во минут: ");
                time.Minutes = int.Parse(Console.ReadLine());

                Console.WriteLine("Введи колв-во секунд: ");
                time.Seconds = int.Parse(Console.ReadLine());

                time.Print();

                Console.WriteLine("Введите на сколько хотите изменить кол-во секунд: ");
                time.ChangeSeconds(int.Parse(Console.ReadLine()));
                time.Print();

                Console.WriteLine("Введите на сколько хотите изменить кол-во минут: ");
                time.ChangeMinutes(int.Parse(Console.ReadLine()));
                time.Print();

                Console.WriteLine("Введите на сколько хотите изменить кол-во часов: ");
                time.ChangeHours(int.Parse(Console.ReadLine()));
                time.Print();
            }
            catch(Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
            
        }
    }
}
