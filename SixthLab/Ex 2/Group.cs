﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Ex_2
{
    public class Group : IList<Student>
    {
        #region Constants
        private const string DEFAULT_NAME = "Group";
        #endregion

        #region Attributes
        private string name;
        private IList<Student> students { set; get; }
        #endregion

        #region Constructors
        public Group() : this(DEFAULT_NAME) { }
    
        public Group(string name) : this(name, new List<Student>()) { }
                  
        public Group(string name, IList<Student> students)
        {
            this.Name = name;
            this.students = new List<Student>(students);
        }

        public Group(string name, params Student[] students)
        {
            this.name = name;
            this.students = new List<Student>(students);
        }
        #endregion

        #region Methods
        public int Count => students.Count;

        public bool IsReadOnly => students.IsReadOnly;

        public Student this[int index] { get => students[index]; set => students[index] = value; }

        public int IndexOf(Student item)
        {
            return students.IndexOf(item);
        }

        public void Insert(int index, Student item)
        {
            students.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            students.RemoveAt(index);
        }

        public void Add(Student student)
        {
            students.Add(student);
        }

        public void Clear()
        {
            students.Clear();
        }

        public bool Contains(Student item)
        {
            return students.Contains(item);
        }

        public void CopyTo(Student[] array, int arrayIndex)
        {
            students.CopyTo(array, arrayIndex);
        }

        public bool Remove(Student item)
        {
            return students.Remove(item);
        }

        public IEnumerator<Student> GetEnumerator()
        {
            return students.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return students.GetEnumerator();
        }
        #endregion

        #region Properties
        public string Name
        {
            set
            {
                name = value;
            }
            get
            {
                return name;
            }
        }
        #endregion
    }
}
