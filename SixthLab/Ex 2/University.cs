﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_2
{
    class University
    {
        #region Constants
        private const string DEFAULT_NAME = "";
        #endregion

        #region Attributes
        private string name;
        private Faculty faculty;
        private Group[] travelGroups;
        #endregion

        #region Constructors
        public University() : this(DEFAULT_NAME) { }

        public University(string name) : this(name, null) { }

        public University(string name, Faculty faculty)
        {
            this.name = name;
            this.faculty = faculty;
        }
        #endregion

        #region Methods
        public void CreateTravelGroups(int countGroups)
        {
            travelGroups = new Group[countGroups];
            SortedSet<Student> bestStudents = faculty.GetBestStudents();

            for (int i = 0; i < countGroups; i++)
            {
                travelGroups[i] = new Group($"Группа {i + 1}");
            }

            int j = 0;

            foreach (Student student in bestStudents)
            {
                if (j == countGroups)
                {
                    j = 0;
                }

                travelGroups[j++].Add(student);
            }
        }
        #endregion

        #region Properties
        public string Name
        {
            set
            {
                name = value;
            }
            get
            {
                return name;
            }
        }

        public Faculty Faculty
        {
            set
            {
                faculty = value;
            }
            get
            {
                return faculty;
            }
        }
        #endregion
    }
}
