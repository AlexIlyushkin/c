﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics;

namespace Ex_2
{
    class Program
    {
        static void Main(string[] args)
        {
            University psuti = new University("PSUTI", new Faculty("FIST"));
            psuti.Faculty.Students = new SortedSet<Student>(StudentManager.SortBySecondName)
            {
                new Student("Alex", "Ilyushkin", Student.SpecialityType.РПИС, 1, true),
                new Student("Ann", "Alexandrova", Student.SpecialityType.ПО, 2, true),
                new Student("Ilya", "Blinov", Student.SpecialityType.ПО, 3, false),
                new Student("Leonid", "Dudka", Student.SpecialityType.РПИС, 4, true),
                new Student("Yaroslav", "Mostugin", Student.SpecialityType.УИТС, 5, false),
                new Student("Stepan", "Mironchev", Student.SpecialityType.УИТС, 6, false),
                new Student("Andrey", "Lovachev", Student.SpecialityType.ПО, 7, true),
                new Student("Vladislav", "Korobeynikov", Student.SpecialityType.УИТС, 8, true),
                new Student("Larina", "Valeriya", Student.SpecialityType.РПИС, 9, true),
                new Student("Alex", "Shabalkin", Student.SpecialityType.ПО, 10, true)
            };

            Console.WriteLine("Введите количество создаваемых групп: ");
            psuti.CreateTravelGroups(int.Parse(Console.ReadLine()));
        }
    }
}
