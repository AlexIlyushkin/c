﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_2
{
    class StudentManager
    {        
        #region Properties
        public static IComparer<Student> SortBySpeciality
        {
            get { return new SpecialityComparer(); }
        }

        public static IComparer<Student> SortBySecondName
        {
            get { return new SecondNameComparer(); }
        }
        #endregion

        #region Comparers
        class SpecialityComparer : IComparer<Student>
        {
            int IComparer<Student>.Compare(Student firstStudent, Student secondStudent)
            {
                if (firstStudent.Speciality > secondStudent.Speciality || firstStudent.Speciality == secondStudent.Speciality)
                {
                    return 1;
                }
                else if (firstStudent.Speciality < secondStudent.Speciality)
                {
                    return -1;
                }
                else return 0;
            }
        }

        class SecondNameComparer : IComparer<Student>
        {
            int IComparer<Student>.Compare(Student firstStudent, Student secondStudent)
            {
                if (firstStudent.SecondName[0] == secondStudent.SecondName[0])
                {
                    for (int i = 1; i < firstStudent.SecondName.Length && i < secondStudent.SecondName.Length; i++)
                    {
                        if (firstStudent.SecondName[i] == secondStudent.SecondName[i])
                        {
                            continue;
                        }
                        else if (firstStudent.SecondName[i] > secondStudent.SecondName[i])
                        {
                            return 1;
                        }
                        else return -1;
                    }
                    return 1;
                }
                else if (firstStudent.SecondName[0] > secondStudent.SecondName[0])
                {
                    return 1;
                }
                else if (firstStudent.SecondName[0] < secondStudent.SecondName[0])
                {
                    return -1;
                }
                else return 0;
            }
        }
        #endregion
    }
} 
