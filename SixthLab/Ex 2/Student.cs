﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Ex_2
{
    public class Student
    {
        #region Constants
        private const string DEAFAULT_FIRST_NAME = "First Name";
        private const string DEAFAULT_SECOND_NAME = "Second Name";
        private const SpecialityType DEAFAULT_SPEC = 0;
        private const int DEFAULT_ID = 0;
        private const bool DEFAULT_BLATNOY = false;
        #endregion

        #region Attributes
        private string firstName;
        private string secondName;
        private SpecialityType speciality;
        private int id;
        private bool blatnoy; //:D
        #endregion

        #region Constructors
        public Student() : this(DEAFAULT_FIRST_NAME, DEAFAULT_SECOND_NAME, DEAFAULT_SPEC, DEFAULT_ID, DEFAULT_BLATNOY) { }

        public Student(string firstName, string secondName, SpecialityType speciality, int id, bool blatnoy)
        {
            FirstName = firstName;
            SecondName = secondName;
            Speciality = speciality;
            ID = id;
            this.blatnoy = blatnoy;
        }
        #endregion

        #region Properties
        public string FirstName
        {
            set
            {
                firstName = value;
            }
            get
            {
                return firstName;
            }
        }
    
        public string SecondName
        {
            set
            {
                secondName = value;
            }
            get
            {
                return secondName;
            }
        }

        public enum SpecialityType
        {
            NULL,
            РПИС,
            ПО,
            УИТС,
            УИ,
            ТП,
            ИСТ
        }
       
        public SpecialityType Speciality
        {
            set
            {
                speciality = value;
            }
            get
            {
                return speciality;
            }
        }

        public int ID
        {
            set
            {
                if(value > 0)
                {
                    id = value;
                }
                else
                {
                    throw new ArgumentException("ID can't has negative value");
                }
            }
            get
            {
                return id;
            }
        }
        
        public bool isBlatnoy
        {
            get { return blatnoy; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return $"\n*студент*\nспециальность: {speciality}\nимя: {firstName}\nфамилия: {secondName}";
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        #endregion
    }
}
