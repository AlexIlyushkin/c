﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex_2
{
    class Faculty
    {
        #region Constants
        private const string DEFAULT_NAME = "Faculty";
        #endregion

        #region Attributes
        private string name;
        private SortedSet<Student> students;
        #endregion

        #region Constructors
        public Faculty() : this(DEFAULT_NAME) { }

        public Faculty(string name)
        {
            this.name = name;
            this.students = new SortedSet<Student>(StudentManager.SortBySecondName);
        }

        public Faculty(string  name, params Student[] students)
        {
            this.name = name;
            this.students = new SortedSet<Student>(students, StudentManager.SortBySecondName);
        }

        public Faculty(string name, IList<Student> students)
        {
            this.name = name;
            this.students = new SortedSet<Student>(students, StudentManager.SortBySecondName);
        }
        #endregion

        #region Methods
        public void AddStudent(Student student)
        {
            students.Add(student);
        }        

        public SortedSet<Student> GetBestStudents()
        {
           SortedSet<Student> bestStudents = new SortedSet<Student>(StudentManager.SortBySpeciality);

            foreach (Student student in students)
            {
                if (student.isBlatnoy)
                {
                    bestStudents.Add(student);
                }
            }

            return bestStudents;
        }
        #endregion

        #region Properties
        public string Name
        {
            set
            {
                name = value;
            }
            get
            {
                return name;
            }
        }

        public SortedSet<Student> Students
        {
            set
            {
                students = value;
            }
            get
            {
                return students;
            }
        }
        #endregion
    }
}
