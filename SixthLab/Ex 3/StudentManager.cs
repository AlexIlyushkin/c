﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

namespace Ex_3
{
    class StudentManager
    {
        private List<Student> StudentList;

        private double averageMarkMathematic;
        private double averageMarkPhysics;
        private double averageMarkInformatics;
        private double maxAverageMark;

        public void ParseFile(string inFileName)
        {
            StreamReader inFileReader = new StreamReader(inFileName, Encoding.GetEncoding(1251));

            StudentList = new List<Student>();

            int countStudents = int.Parse(inFileReader.ReadLine());

            for (int i = 0; i < countStudents; i++)
            {
                StudentList.Add(ParseLine(inFileReader.ReadLine()));

                averageMarkMathematic += StudentList[i].MarkMathematic;
                averageMarkPhysics += StudentList[i].MarkPhysics;
                averageMarkInformatics += StudentList[i].MarkInformatics;

                if (StudentList[i].AverageMark > maxAverageMark)
                {
                    maxAverageMark = StudentList[i].AverageMark;
                }
            }

            averageMarkMathematic /= StudentList.Count;
            averageMarkPhysics /= StudentList.Count;
            averageMarkInformatics /= StudentList.Count;

            inFileReader.Close();
        }

        private Student ParseLine(string line)
        {
            string[] infoStudent = line.Split(' ');
            return new Student(infoStudent[0], infoStudent[1], int.Parse(infoStudent[2]), int.Parse(infoStudent[3]), int.Parse(infoStudent[4]));
        }

        public void WriteBestStudentToFile(string outFileName)
        {
            StreamWriter outFileWriter = new StreamWriter(outFileName, false, Encoding.UTF8);

            for (int i = 0; i < StudentList.Count; i++)
            {
                if (StudentList[i].AverageMark == maxAverageMark)
                {
                    outFileWriter.WriteLine(StudentList[i].ToString());
                }
            }

            outFileWriter.Close();
        }        

        public double AverageMarkMathematic
        {
            get
            {
                return averageMarkMathematic;
            }
        }

        public double AverageMarkPhysics
        {
            get
            {
                return averageMarkPhysics;
            }
        }

        public double AverageMarkInformatics
        {
            get
            {
                return averageMarkInformatics;
            }
        }

        public double MaxAverageMark
        {
            get
            {
                return maxAverageMark;
            }
        }              
    }
}
