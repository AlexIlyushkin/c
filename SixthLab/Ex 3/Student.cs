﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Ex_3
{
    class Student
    {
        private const string DEFAULT_FNAME = "";
        private const string DEFAULT_SNAME = "";
        private const int DEFAULT_MATH_MARK = 0;
        private const int DEFAULT_PHYS_MARK = 0;
        private const int DEFAULT_INF_MARK = 0;

        private string firstName;
        private string secondName;
        private int markMathematic;
        private int markPhysics;
        private int markInformatics;
        private double averageMark;

        public Student() : this(DEFAULT_FNAME, DEFAULT_SNAME, DEFAULT_MATH_MARK, DEFAULT_PHYS_MARK, DEFAULT_INF_MARK) { }

        public Student(string firstName, string secondName, int markMath, int markPhys, int markInf)
        {
            this.SecondName = secondName;
            this.FirstName = firstName;
            MarkMathematic = markMath;
            MarkPhysics = markPhys;
            MarkInformatics = markInf;
            AverageMark = (double)(MarkMathematic + MarkPhysics + MarkInformatics) / 3;
        }

        public string FirstName
        {
            set
            {
                firstName = value;
            }
            get
            {
                return firstName;
            }
        }

        public string SecondName
        {
            set
            {
                secondName = value;
            }
            get
            {
                return secondName;
            }
        }

        public int MarkMathematic
        {
            set
            {
                if(value <= 5 && value >= 2)
                {
                    markMathematic = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Оценка может быть только от 2 до 5");
                }
                
            }
            get
            {
                return markMathematic;
            }
        }

        public int MarkPhysics
        {
            set
            {
                if (value <= 5 && value >= 2)
                {
                    markPhysics = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Оценка может быть только от 2 до 5");
                }
            }
            get
            {
                return markPhysics;
            }
        }

        public int MarkInformatics
        {
            set
            {
                if (value <= 5 && value >= 2)
                {
                    markInformatics = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Оценка может быть только от 2 до 5");
                }
            }
            get
            {
                return markInformatics;
            }
        }

        public double AverageMark
        {
            set
            {
                averageMark = value;
            }
            get
            {
                return averageMark;
            }
        }        
      
        public override string ToString()
        {
            return $"{firstName} {secondName} {markMathematic} {markPhysics} {markInformatics} {averageMark:f1}";
        }
    }
}
