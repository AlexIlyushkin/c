﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

namespace Ex_3
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                StudentManager studentManager = new StudentManager();
                studentManager.ParseFile("in.txt");

                Console.WriteLine("Средний балл всех учащихся по матеше: " + studentManager.AverageMarkMathematic);
                Console.WriteLine("Средний балл всех учащихся по физике: " + studentManager.AverageMarkPhysics);
                Console.WriteLine("Средний балл всех учащихся по инфе: " + studentManager.AverageMarkInformatics);

                studentManager.WriteBestStudentToFile("out.txt");
            }
            catch(ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
            }
            
        }
    }
}
